stupan (1.17.4-1) jammy; urgency=high

  * Move inventory tabs under common master tab.
  * Enable sorting on money accounts.
  * CRITICAL: Fix bug in cointube counting.

 -- Hugo Hörnquist <hugo@lysator.liu.se>  Thu, 30 Nov 2023 00:24:22 +0100

stupan (1.17.3-1) jammy; urgency=medium

  * Add preview when transfering money between accounts
  * Replace money diff system with a new one.
  * Show hidden products when searching by barcode.
  * Add Doxygen documentation to everything public.
  * General cleanup.

 -- Hugo Hörnquist <hugo@lysator.liu.se>  Thu, 23 Nov 2023 18:30:54 +0100

stupan (1.17.2-1) jammy; urgency=medium

  * Add dependency on sqlite-to-cpp.
    This greatly helps with magic numbers in the code, and will hopefully
    prevent bugs in the future.
  * General cleanup of magic numbers and similar
  * Add categories to the .desktop files.
  * Fix the acquisitions menu.
  * Fix acquisitions {not_,}for_sale being mixed up.
  * Add clarifying text about money transfers.
  * Allow stockcheck sort by column.
  * Add program information to program
    This makes it easer to check what version is currently running.
    It also opens up for automatic database migrations from the next
    version and forward.
  * Add tableprinter to debian copyright
  *  Add debian description of package.
  * General cleanup.

 -- Hugo Hörnquist <hugo@lysator.liu.se>  Thu, 27 Apr 2023 16:24:04 +0200

stupan (1.17.1-1) jammy; urgency=medium

  * Allow adding accounts, and adding money to them.
  * Minor cleanups

 -- Hugo Hörnquist <hugo@lysator.liu.se>  Thu, 20 Apr 2023 18:06:53 +0200

stupan (1.17-1) jammy; urgency=medium

  * Fix errors introduced my 1.16
    * Repair money diff.
    * Repair and rebuild stock diff view.
  * Fixes for older bugs
    * "Repair" View of saved SQL queries.
  * Other improvements
    * Fix deprecation notices.
    * Move many db queries into resource files.
    * Change margin for printed pages to be more printer friendly.

 -- Hugo Hörnquist <hugo@lysator.liu.se>  Sun, 16 Apr 2023 21:10:05 +0200

stupan (1.16-1) jammy; urgency=medium

  * Properly load translations from system path.
  * Fix time interval for displaying easter egg.
  * Limit running instances to 1.
  * Properly introduce cash drawer open count.
  * Add account system.

 -- Hugo Hörnquist <hugo@lysator.liu.se>  Sun, 16 Apr 2023 21:07:18 +0200

stupan (1.15-1) jammy; urgency=medium

  * Re-implement rune easter egg. 

 -- Hugo Hörnquist <hugo@lysator.liu.se>  Thu, 30 Mar 2023 18:46:03 +0200

stupan (1.14.1-1) una; urgency=medium

  * debian changelog to better reflect reality.

 -- Hugo Hörnquist <hugo@lysator.liu.se>  Fri, 05 Aug 2022 17:49:30 +0200

stupan (1.14-1) unstable; urgency=medium

  * Make .desktop files runable.
  * Add license-blocks to debian/copyright.
  * Replace home written serial library with Qt's
  * Replace hand-written makefiles with propper Qmake files.
  * Various code cleanups.

 -- Hugo Hörnquist <hugo@lysator.liu.se>  Sun, 03 Apr 2022 04:16:25 +0200

stupan (1.13.42) unstable; urgency=medium

  * Fix path

 -- Hugo Hörnquist <hugo@lsator.liu.se>  Fri, 01 Apr 2022 17:43:23 +0200

stupan (1.13.41-1) unstable; urgency=medium

  * April fools update

 -- Hugo Hörnquist <hugo@lysator.liu.se>  Fri, 01 Apr 2022 17:23:04 +0200

stupan (1.13-1) unstable; urgency=medium

  * Update make install to honor DESTDIR.
  * Revert "Fix deprecation notice for foreach."
  * Update makefile to make debian happy.
  * Update desktop files for new paths.
  * License code under GPL3+.

 -- Hugo Hörnquist <hugo@lysator.liu.se>  Fri, 11 Feb 2022 18:36:56 +0100

stupan (1.12.4-2) unstable; urgency=low

  * Initial release as package.

 -- Hugo Hörnquist <hugo@lysator.liu.se>  Fri, 11 Feb 2022 18:35:49 +0100

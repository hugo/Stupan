#ifndef DRAINAGE_H
#define DRAINAGE_H

#include <QWidget>
#include <QDateTime>

namespace Ui {
class Drainage;
}

/**
 * @brief Controller for the drainage administration tab
 *
 * Drainages allow products to dynamically change price
 * getting progressively cheaper.
 *
 * @todo the remove drainage instance button adds the id column to the futures table
 */
class Drainage : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief Initialize the widget
     * @param parent Qt parent for cleanup.
     */
    explicit Drainage(QWidget *parent = nullptr);

    /**
     * @brief cleanup
     */
    ~Drainage();

private:
    Ui::Drainage *ui;

private slots:
    void on_drainageAddButton_clicked();
    void on_drainageTimeZoneComboBox_currentIndexChanged(int index);
    void on_drainageStartTimeDateTimeEdit_dateTimeChanged(const QDateTime &dateTime);
    void on_drainageRemoveButton_clicked();
    void on_drainageCancelButton_clicked();
    void on_drainageUpdateButton_clicked();


private:
    void updateDrainageView();
    void updateCurrentDrainageStatus();
};

#endif // DRAINAGE_H

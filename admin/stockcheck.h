#ifndef STOCKCHECK_H
#define STOCKCHECK_H

#include <QWidget>
#include <QVariant>

namespace Ui {
class StockCheck;
}

/**
 * @brief Controller for the stock check tab.
 *
 * This tab allows taking inventory of both the stock, and the money.
 */
class StockCheck : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief Initializes the widget.
     * Creates the database view diff_view, along with a trigger which propagates changes
     * into stock_diff.
     * @param parent For Qt cleanup.
     */
    explicit StockCheck(QWidget *parent = nullptr);
    /**
     * @brief cleanup
     */
    ~StockCheck();

private:
    Ui::StockCheck *ui;
    void clear_stockDiffTemp();
    void setMoneyDiffLabels(QVariant, int);
    int setMoneyDiffLabels(QVariant);
    void setMoneyDiffLabels();

private slots:
    void recalculate_diff_column(const QModelIndex& topLeft, const QModelIndex& bottomRight, const QVector<int>& roles);
    void on_inventoryStockDiffSubmit_clicked();
    void on_inventoryStockDiffReset_clicked();
    void on_inventoryQuickInventoryCheckBox_toggled(bool checked);

};

#endif // STOCKCHECK_H

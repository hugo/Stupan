CREATE TABLE IF NOT EXISTS sql_queries (
    id INTEGER PRIMARY KEY NOT NULL,
    name TEXT NOT NULL UNIQUE,
    query TEXT DEFAULT '' NOT NULL
)

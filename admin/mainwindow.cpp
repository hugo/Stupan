#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtCore>

// acquisitions menu should allow power usage
// - search auto clear?
// - sugested buy from systemet (this is replaced by reading from done acquisitions)

// TODO money diff doesn't update when cash drawer changes elsewhere in program
//      it however still inserts the correct data into the db
// This binds into most strings not auto updating, and some more needing manual updates

// TODO stock diff need to exit field before submitting
//      This can lead to a missed field

/*
 * All internal prices should be in öre
 * Never trust a front end number unless you KNOW it's good
 */

MainWindow::MainWindow(const Configuration &conf, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->drawer = new CashDrawer(conf.port);
    QObject::connect(this, &MainWindow::open_till,
                     this->drawer, &CashDrawer::open_till);
    QObject::connect(ui->acquisitionsTab, &Acquisitions::open_till,
                     this, &MainWindow::open_till);

    QObject::connect(this, &MainWindow::accounts_changed,
                     ui->moneyTab, &MoneyTab::setMoneyAccountValues);
    QObject::connect(this, &MainWindow::accounts_changed,
                     ui->moneyTab, &MoneyTab::reloadTransferHistory);

    QObject::connect(this, &MainWindow::accounts_changed,
                     findChild<MoneyCheck*>("MoneyCheck"),
                     &MoneyCheck::updateMoney);

    QObject::connect(ui->acquisitionsTab, &Acquisitions::accounts_changed,
                     this, &MainWindow::accounts_changed);
    QObject::connect(ui->moneyTab, &MoneyTab::accounts_changed,
                     this, &MainWindow::accounts_changed);

    /*
     * This updates acquisitions with the new names when products are edited.
     * TODO inventeringssystemet uppdateras inte här, bör det göra det?
     * TODO inte heller lager-status, dock har den en refresh knapp.
     */
    QObject::connect(ui->productListTab, &ProductList::products_changed,
                     ui->acquisitionsTab, &Acquisitions::update_products);
    QObject::connect(ui->productListTab, &ProductList::new_product,
                     ui->acquisitionsTab, &Acquisitions::reset_search);
}

MainWindow::~MainWindow()
{
    delete this->drawer;
    delete ui;
}

void MainWindow::on_openTillButton_clicked()
{
    emit open_till();
}


#ifndef MONEYCHECK_H
#define MONEYCHECK_H

#include <QWidget>
#include <array>

namespace Ui {
class MoneyCheck;
}

class MoneyCheck : public QWidget
{
    Q_OBJECT

public:
    explicit MoneyCheck(QWidget *parent = nullptr);
    ~MoneyCheck();

public slots:
    void updateMoney();

private slots:
    /** Reset all entered cash values
     *  This includes all individual denominations,
     *  as well as the grand total
     */
    void resetCash();

    /** Reset all entered coin tube values
     *  This includes all individual denomination groups,
     *  as well as the grand total */
    void resetCointube();

    /** Reset all fields */
    void resetAll();


    void on_sumCash_valueChanged(int arg1);
    void on_sumCointube_valueChanged(int arg1);
    void on_sumCash_editingFinished();
    void on_sumCointube_editingFinished();
    void on_submitButton_clicked();
    void on_sumTotal_editingFinished();
    void on_sumTotal_valueChanged(int arg1);

    /** Set the expected amount of money in the current account */
    inline void setExpected(long long int x) {
        this->expected = x;
        emit MoneyCheck::expectedChanged(x);
    }

    /** Update the diff between actual and expected money counts */
    void updateDiff();

    /**
     *  Select an account if none is selected, then
     *  Load balance for current account, updating things along the way
     */
    void loadStuff();

    /** Reload the expected balance */
    void reloadExpected(const QString& account);

    void on_pushButton_clicked();

signals:
    void expectedChanged(long long int);

private:
    Ui::MoneyCheck *ui;
    long long int sum_cash();
    long long int sum_cointube();
    long long expected = 0;
};

#endif // MONEYCHECK_H

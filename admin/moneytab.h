#ifndef MONEYTAB_H
#define MONEYTAB_H

#include <QWidget>
#include <QVariant>
#include <QLabel>

namespace Ui {
class MoneyTab;
}

/**
 * @brief Controller for the money administration tab.
 */
class MoneyTab : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief Sets up the widget.
     * Recreates the database view money_transfers_simple.
     * This view is NOT deleted on object destruction.
     * @param parent for Qt cleanup.
     */
    explicit MoneyTab(QWidget *parent = nullptr);
    /**
     * @brief cleanup
     */
    ~MoneyTab();

public slots:
    /**
     * @brief requery the database for current values.
     *
     * Updates the list of accounts to reflect the current set of accounts,
     * and updates the balance of each account.
     *
     * Also adds a few "virtual" accounts.
     */
    void setMoneyAccountValues();

    /**
     * @brief Reload the list of past transactions from the DB.
     */
    void reloadTransferHistory();

signals:
    /**
     * @brief One or more accounts changed.
     */
    void accounts_changed();

private:
    Ui::MoneyTab *ui;
    void setAccTransferCombo();

    void updateMoneyPreview(const QString& accountName,
                            QLabel *accLabel,
                            QLabel *oldLabel,
                            QLabel *toLabel,
                            // std::binary_function<double, double, double>
                            // double (*f) (double, double)
                            std::function<double(double, double)> const& f
                            );
    void updateFromPreview(const QString& accountName);
    void updateToPreview(const QString& accountName);

private slots:
    void on_moneyAccTransferSubmit_clicked();
    void on_moneyAccUpdateBtn_clicked();
    void on_moneyAccNewAccBtn_clicked();

    void on_moneyAccTransferAmonut_valueChanged(double);
    void on_moneyAccTransferFrom_currentIndexChanged(const QString&);
    void on_moneyAccTransferTo_currentIndexChanged(const QString&);
};

#endif // MONEYTAB_H

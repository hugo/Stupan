CREATE TEMPORARY TRIGGER propagate_products_with_stock
INSTEAD OF UPDATE ON products_with_stock
FOR EACH ROW
BEGIN
UPDATE products
   SET sorting_id = NEW.sorting_id
     , buy_id = NEW.buy_id
     , bar_code = NEW.bar_code
     , name = NEW.name
     , price = NEW.price
     , sale_status = NEW.sale_status
WHERE id = NEW.id;
END

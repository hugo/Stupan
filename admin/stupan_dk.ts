<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>Acquisitions</name>
    <message>
        <location filename="acquisitions.ui" line="36"/>
        <source>Sök...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="acquisitions.ui" line="53"/>
        <source>Lägg till</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="acquisitions.ui" line="71"/>
        <source>Id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="acquisitions.ui" line="76"/>
        <location filename="acquisitions.cpp" line="54"/>
        <source>Namn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="acquisitions.ui" line="81"/>
        <source>Antal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="acquisitions.ui" line="86"/>
        <source>Pris</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="acquisitions.ui" line="91"/>
        <source>Totalt Pris</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="acquisitions.ui" line="101"/>
        <source>Totalt Inköpspris (kr):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="acquisitions.ui" line="130"/>
        <source>Ta bort markerad</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="acquisitions.ui" line="159"/>
        <source>Submit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="acquisitions.cpp" line="42"/>
        <source>Till salu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="acquisitions.cpp" line="43"/>
        <source>Ej till salu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="acquisitions.cpp" line="44"/>
        <source>Dold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="acquisitions.cpp" line="55"/>
        <source>Försäljningspris</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="acquisitions.cpp" line="56"/>
        <source>Försäljningsstatus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="acquisitions.cpp" line="134"/>
        <source>%L1</source>
        <comment>Totalsumma för transaktion</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="acquisitions.cpp" line="244"/>
        <source>Inmatningen misslyckades</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="acquisitions.cpp" line="245"/>
        <source>Misslyckades med att lägga till inköpet i databasen.

SQLFel: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Drainage</name>
    <message>
        <location filename="drainage.ui" line="8"/>
        <source>Tömning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="drainage.ui" line="32"/>
        <source>Sluttid:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="drainage.ui" line="42"/>
        <source>Minimipris:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="drainage.ui" line="52"/>
        <source>Tidszon:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="drainage.ui" line="62"/>
        <location filename="drainage.ui" line="95"/>
        <source>yyyy-MM-dd hh:mm t</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="drainage.ui" line="72"/>
        <source>Sänkning/tid:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="drainage.ui" line="82"/>
        <source>Hur ofta (min):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="drainage.ui" line="108"/>
        <location filename="drainage.ui" line="148"/>
        <source> kr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="drainage.ui" line="125"/>
        <source>Local Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="drainage.ui" line="130"/>
        <source>UTC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="drainage.ui" line="138"/>
        <source>Starttid:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="drainage.ui" line="160"/>
        <source>Skapa Tömningstillfälle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="drainage.ui" line="176"/>
        <source>Pågaende tömning:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="drainage.ui" line="203"/>
        <source>Uppdatera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="drainage.ui" line="213"/>
        <source>Avbryt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="drainage.ui" line="245"/>
        <source>Kommande Tömningar (lokal tid):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="drainage.ui" line="266"/>
        <source>Gånga tömmningar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="drainage.ui" line="280"/>
        <source>Ta Bort Tömningstillfälle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="drainage.cpp" line="41"/>
        <location filename="drainage.cpp" line="78"/>
        <source>Starttid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="drainage.cpp" line="42"/>
        <location filename="drainage.cpp" line="79"/>
        <source>Sluttid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="drainage.cpp" line="43"/>
        <location filename="drainage.cpp" line="80"/>
        <source>Kr/Tid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="drainage.cpp" line="44"/>
        <location filename="drainage.cpp" line="81"/>
        <source>Hur ofta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="drainage.cpp" line="45"/>
        <location filename="drainage.cpp" line="82"/>
        <source>Minimipris</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="drainage.cpp" line="239"/>
        <source>%1 kr/%2 min, ner till %3 kr
Mellan %4 och %5 lokal tid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="drainage.cpp" line="247"/>
        <source>Ingen pågående tömning</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Admin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="38"/>
        <source>Produktlista</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="43"/>
        <source>Inköp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="48"/>
        <source>Invertering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="53"/>
        <source>Tömning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="58"/>
        <source>Pengar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="63"/>
        <source>SQL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="68"/>
        <source>Övrigt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="87"/>
        <source>Öppna Kassan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="108"/>
        <source>Om</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MoneyTab</name>
    <message>
        <location filename="moneytab.ui" line="36"/>
        <source> kr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="moneytab.ui" line="62"/>
        <location filename="moneytab.cpp" line="45"/>
        <source>Till</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="moneytab.ui" line="99"/>
        <source>Notering (anledning, person, ...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="moneytab.ui" line="122"/>
        <location filename="moneytab.cpp" line="44"/>
        <source>Från</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="moneytab.ui" line="132"/>
        <source>**Vid privata utlägg**: Flytta pengar från rimligt konto (kassalådan eller ditt skuldkonto) till &quot;Inget Konto&quot; kontot.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="moneytab.ui" line="151"/>
        <location filename="moneytab.cpp" line="27"/>
        <source>Kronor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="moneytab.ui" line="167"/>
        <source>Submit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="moneytab.ui" line="215"/>
        <source>Uppdatera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="moneytab.ui" line="230"/>
        <source>Kontonamn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="moneytab.ui" line="243"/>
        <source>Lägg till konto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="moneytab.ui" line="259"/>
        <source>Forna Överföringar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="moneytab.cpp" line="26"/>
        <source>Namn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="moneytab.cpp" line="43"/>
        <source>Tid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="moneytab.cpp" line="46"/>
        <source>Summa</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="moneytab.cpp" line="47"/>
        <source>Notering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="moneytab.cpp" line="106"/>
        <source>Misslyckades skapa konto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="moneytab.cpp" line="107"/>
        <source>Konto med det namnet finns troligen redan, alternativt har SQLite dåliga felmeddelanden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="moneytab.cpp" line="127"/>
        <source>Lager (projicerat)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="moneytab.cpp" line="132"/>
        <source>Lager (inköp)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="moneytab.cpp" line="141"/>
        <source>Totalt (projicerat)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProductList</name>
    <message>
        <location filename="productlist.ui" line="9"/>
        <source>Sök...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="productlist.ui" line="55"/>
        <source>Ta bort markerad</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="productlist.ui" line="62"/>
        <source>Ny produkt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="productlist.ui" line="76"/>
        <source>Visa dolda produkter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="productlist.cpp" line="37"/>
        <source>id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="productlist.cpp" line="38"/>
        <source>Sorterings-ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="productlist.cpp" line="39"/>
        <source>Systembolaget ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="productlist.cpp" line="40"/>
        <source>Streckkod</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="productlist.cpp" line="41"/>
        <source>Namn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="productlist.cpp" line="42"/>
        <source>Pris</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="productlist.cpp" line="43"/>
        <source>Till Salu?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="productlist.cpp" line="44"/>
        <source>Lager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="productlist.cpp" line="64"/>
        <source>Till salu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="productlist.cpp" line="65"/>
        <source>Ej till salu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="productlist.cpp" line="66"/>
        <source>Dold</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SQLTab</name>
    <message>
        <location filename="sqltab.ui" line="25"/>
        <source>Resultat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sqltab.ui" line="51"/>
        <source>Skriv ut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sqltab.ui" line="59"/>
        <source>Sparade</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sqltab.ui" line="93"/>
        <source>Ta bort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sqltab.ui" line="107"/>
        <source>&quot;filnamn&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sqltab.ui" line="120"/>
        <source>SQL förfrågan...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sqltab.ui" line="127"/>
        <source>Spara</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sqltab.ui" line="134"/>
        <source>Ladda</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sqltab.ui" line="141"/>
        <source>Kör</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sqltab.cpp" line="34"/>
        <source>id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sqltab.cpp" line="35"/>
        <source>Namn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sqltab.cpp" line="36"/>
        <source>SQL Fråga</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sqltab.cpp" line="79"/>
        <source>SQLmisslyckande</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sqltab.cpp" line="80"/>
        <source>Misslyckades att ställa förfrågan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sqltab.cpp" line="91"/>
        <source>Kan inte starta skrivare</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StockCheck</name>
    <message>
        <location filename="stockcheck.ui" line="9"/>
        <source>Inventariediff</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="stockcheck.ui" line="23"/>
        <source>För uppdaterade produktnamn, återställ tabellen.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="stockcheck.ui" line="32"/>
        <source>Snabbläge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="stockcheck.ui" line="52"/>
        <source>Återställ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="stockcheck.ui" line="65"/>
        <location filename="stockcheck.ui" line="188"/>
        <source>Submit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="stockcheck.ui" line="85"/>
        <source>Pengadiff:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="stockcheck.ui" line="110"/>
        <source>Förväntad:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="stockcheck.ui" line="117"/>
        <location filename="stockcheck.ui" line="159"/>
        <source>X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="stockcheck.ui" line="131"/>
        <source>Faktiskt:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="stockcheck.ui" line="138"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="stockcheck.ui" line="152"/>
        <source>Diff:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="stockcheck.ui" line="181"/>
        <source>Ladda om</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="stockcheck.cpp" line="34"/>
        <source>id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="stockcheck.cpp" line="35"/>
        <source>sale status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="stockcheck.cpp" line="36"/>
        <source>Namn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="stockcheck.cpp" line="37"/>
        <source>Förväntat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="stockcheck.cpp" line="38"/>
        <source>Faktiskt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="stockcheck.cpp" line="39"/>
        <source>Diff</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="stockcheck.cpp" line="197"/>
        <location filename="stockcheck.cpp" line="216"/>
        <source>%1 kr</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>

CREATE TEMPORARY VIEW diff_view AS
SELECT p.id AS product_id,
       p.sale_status AS sale_status,
       -- visible border
       p.name AS name,
       s.total AS expected,
       NULL AS actual,
       NULL AS diff
FROM products p
INNER JOIN stock_balance s ON p.id = s.product_id

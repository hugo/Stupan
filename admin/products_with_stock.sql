CREATE TEMPORARY VIEW products_with_stock AS
SELECT p.id
     , p.sorting_id
     , p.buy_id
     , p.bar_code
     , p.name
     , p.price
     , p.sale_status
     , s.total
FROM products p
LEFT JOIN stock_balance s ON p.id = s.product_id

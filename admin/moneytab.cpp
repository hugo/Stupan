#include "moneytab.h"
#include "ui_moneytab.h"

#include <QSqlTableModel>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlError>
#include <QMessageBox>
#include <QSqlRecord>

#include <functional>

#include "macros.h"
#include "utils.h"
#include "db_enum.h"

QVariant get_projected_stock_value();
QVariant get_acquistion_stock_value();

MoneyTab::MoneyTab(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MoneyTab)
{
    ui->setupUi(this);

    QSqlTableModel* accModel = new QSqlTableModel(this);
    ui->moneyAccTreeView->setModel(accModel);
    accModel->setTable("money_simple");
    accModel->select();
    SET_HEADER_DATA(accModel,
            { 0, tr("Namn") },
            { 1, tr("Kronor") });
    // Don't actualy try to submit on this table
    accModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
    ui->moneyAccTreeView->header()->setSectionResizeMode(QHeaderView::ResizeToContents);

    setMoneyAccountValues();

    setAccTransferCombo();

    util::runSql("DROP VIEW IF EXISTS money_transfers_simple");
    util::runSqlResource(":/moneytab/view/money_transfers_simple.sql");
    QSqlTableModel* transHistoryModel = new QSqlTableModel(this);
    ui->moneyTransferHistory->setModel(transHistoryModel);
    transHistoryModel->setTable("money_transfers_simple");
    transHistoryModel->select();
    SET_HEADER_DATA(transHistoryModel,
            { DB::MoneyTransfersSimple::Time,    tr("Tid") },
            { DB::MoneyTransfersSimple::FromAcc, tr("Från") },
            { DB::MoneyTransfersSimple::ToAcc,   tr("Till") },
            { DB::MoneyTransfersSimple::Amount,  tr("Summa") },
            { DB::MoneyTransfersSimple::Note,    tr("Notering") });
    // Don't actualy try to submit on this table
    transHistoryModel->setEditStrategy(QSqlTableModel::OnManualSubmit);

    ui->moneyTransferHistory->header()->setSectionResizeMode(QHeaderView::ResizeToContents);
}

MoneyTab::~MoneyTab()
{
    delete ui;
}

void MoneyTab::on_moneyAccTransferSubmit_clicked()
{
    qlonglong amount = ui->moneyAccTransferAmonut->value() * 100ll;
    qlonglong from   = ui->moneyAccTransferFrom->currentData().toLongLong();
    qlonglong to     = ui->moneyAccTransferTo->currentData().toLongLong();
    QString text     = ui->moneyAccTransferText->toPlainText();

    if (amount == 0 || from == to) return;

    qDebug() << amount << from << to << text;

    QSqlQuery query;
    query.prepare("INSERT INTO money_transfers (change, from_acc, to_acc, note) "
                  "VALUES (:amount, :from, :to, :note)");
    query.bindValue(":amount", amount);
    query.bindValue(":from", from);
    query.bindValue(":to", to);
    query.bindValue(":note", text.isEmpty() ? QVariant::String : QVariant(text));
    if (!query.exec())
        qDebug() << __FILE__ << __LINE__ << query.lastError();

    ui->moneyAccTransferAmonut->setValue(0);
    ui->moneyAccTransferFrom->setCurrentIndex(0);
    ui->moneyAccTransferTo->setCurrentIndex(0);
    ui->moneyAccTransferText->setPlainText("");

    // TODO update money values

    emit accounts_changed();
}

void MoneyTab::on_moneyAccUpdateBtn_clicked()
{
    setMoneyAccountValues();
}

void MoneyTab::on_moneyAccNewAccBtn_clicked()
{
    QString name = ui->moneyAccNewAccText->text();
    if (name.isEmpty()) return;

    QSqlQuery query;
    query.prepare("INSERT INTO money (name) VALUES (:name)");
    query.bindValue(":name", name);
    if (!query.exec()) {
        qDebug() << __FILE__ << __LINE__ << query.lastError();
        if (query.lastError().text().contains("UNIQUE")) {
            QMessageBox::warning(this, tr("Misslyckades skapa konto"),
                                 tr("Konto med det namnet finns troligen redan, alternativt har SQLite dåliga felmeddelanden"));
            return;
        }
    }

    ui->moneyAccNewAccText->setText("");

    setMoneyAccountValues();
    setAccTransferCombo();
}


void MoneyTab::setMoneyAccountValues() {
    QSqlTableModel* accModel = static_cast<QSqlTableModel*>(ui->moneyAccTreeView->model());

    accModel->select();

    QSqlRecord rec = accModel->record();

    QVariant projected_price = get_projected_stock_value();
    rec.setValue("name", QVariant(tr("Lager (projicerat)")));
    rec.setValue("amount", QVariant(util::formatMoney(projected_price.toDouble() / 100.0)));
    accModel->insertRecord(-1, rec);

    QVariant buy_price = get_acquistion_stock_value();
    rec.setValue("name", QVariant(tr("Lager (inköp)")));
    rec.setValue("amount", QVariant(util::formatMoney(buy_price.toDouble() / 100.0)));
    accModel->insertRecord(-1, rec);

    QSqlQuery query("SELECT sum(total) FROM account_balances "
                    "WHERE account_id != 0");
    if (query.next()) {
        qlonglong sum = query.value(0).toLongLong()
                        + projected_price.toLongLong();
        rec.setValue("name", QVariant(tr("Totalt (projicerat)")));
        rec.setValue("amount", QVariant(util::formatMoney(static_cast<double>(sum) / 100.0)));
        accModel->insertRecord(-1, rec);
    }
}

void MoneyTab::reloadTransferHistory()
{
    static_cast<QSqlTableModel*>(ui->moneyTransferHistory->model())->select();
}


void MoneyTab::setAccTransferCombo() {

    ui->moneyAccTransferFrom->clear();
    ui->moneyAccTransferTo->clear();

    QSqlQuery accQuery("SELECT id, name FROM money");
    while (accQuery.next()) {
        QString name = accQuery.value(1).toString();
        QVariant value = accQuery.value(0);
        ui->moneyAccTransferFrom->addItem(name, value);
        ui->moneyAccTransferTo->addItem(name, value);
    }
}



QVariant get_acquistion_stock_value()
{
    qlonglong total_price = 0;
    QSqlQuery tQuery("SELECT s.product_id as id, s.total as active, p.name "
                     "FROM stock_balance s INNER JOIN products p "
                     "ON s.product_id = p.id");
    while (tQuery.next()) {
        // qDebug() << __LINE__;
        long long target = tQuery.value("active").toLongLong();

        if (target < 0) continue;

        QSqlQuery query;
        query.prepare("SELECT id, time, product_id, item_price, amount "
                      "FROM acquisitions WHERE product_id = :id "
                      "ORDER BY time DESC");
        query.bindValue(":id", tQuery.value("id"));
        query.exec();

        long long amount = 0;
        long long total_product_price = 0;
        while (query.next()) {
            long long sub_amount = query.value("amount").toLongLong();
            long long item_price = query.value("item_price").toLongLong();
            // qDebug() << __LINE__ << amount << sub_amount;
            if (amount + sub_amount > target) {
                total_product_price += (target - amount) * item_price;
                amount = target;
                break;
            } else {
                amount += sub_amount;
                total_product_price += sub_amount * item_price;
            }

        }
        total_price += total_product_price;
    }

    return QVariant(total_price);
}

QVariant get_projected_stock_value() {
    QSqlQuery query("SELECT SUM(p.price * s.active) "
                    "FROM products p "
                    "INNER JOIN stock s ON "
                    "p.id = s.product_id");
    if (query.next())
        return query.value(0);
    return QVariant::Int;

}

void MoneyTab::updateMoneyPreview(
        const QString& accountName,
        QLabel *accLabel,
        QLabel *oldLabel,
        QLabel *newLabel,
        std::function<double(double, double)> const& proc
) {
    accLabel->setText(accountName);

    QSqlQuery query;
    query.prepare("SELECT amount "
                  "FROM money_simple "
                  "WHERE name = :name");
    query.bindValue(":name", accountName);
    query.exec();
    query.next();
    double currentAmount = query.value("amount").toDouble();
    double change = ui->moneyAccTransferAmonut->value();
    double newAmount = proc(currentAmount, change);

    oldLabel->setText(QString("%1 kr").arg(currentAmount, 0, 'f', 2));
    newLabel->setText(QString("%1 kr").arg(newAmount, 0, 'f', 2));


}


void MoneyTab::updateFromPreview(const QString &accountName) {
    this->updateMoneyPreview(accountName,
                             ui->PreviewFrom,
                             ui->previewFromOld,
                             ui->previewFromNew,
                             std::minus<double>());

}

void MoneyTab::updateToPreview(const QString &accountName) {
    this->updateMoneyPreview(accountName,
                             ui->PreviewTo,
                             ui->previewToOld,
                             ui->previewToNew,
                             std::plus<double>());
}

void MoneyTab::on_moneyAccTransferAmonut_valueChanged(double)
{
    this->updateFromPreview(ui->moneyAccTransferFrom->currentText());
    this->updateToPreview(ui->moneyAccTransferTo->currentText());
}

void MoneyTab::on_moneyAccTransferFrom_currentIndexChanged(const QString &value)
{
    updateFromPreview(value);
}


void MoneyTab::on_moneyAccTransferTo_currentIndexChanged(const QString &value)
{
    updateToPreview(value);
}

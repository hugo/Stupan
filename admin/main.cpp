#include "mainwindow.h"
#include <QApplication>

#include <QLocale>

#include "configuration.h"
#include "utils.h"

int main(int argc, char *argv[])
{
    QLocale::setDefault(QLocale(QLocale::Swedish, QLocale::Sweden));
    QApplication a(argc, argv);

    util::check_exclusivity();

    QString HOME = QString::fromLocal8Bit(getenv("HOME"));

    Configuration conf(HOME + "/.stupan/config");

    util::setupDatabase(conf.db_name);

    MainWindow w(conf);
    w.show();

    return a.exec();
}

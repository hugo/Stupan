#ifndef SQLTAB_H
#define SQLTAB_H

#include <QWidget>

#include <QPrinter>

namespace Ui {
class SQLTab;
}

/**
 * @brief Controller for the SQL tab.
 *
 * The SQL tab allows storing and running arbitrary SQL queries.
 */
class SQLTab : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief Sets up the widget
     * Creates the premanent sql_queries table if it doesn't yet exist.
     * @param parent For Qt Cleanup.
     */
    explicit SQLTab(QWidget *parent = nullptr);
    /**
     * @brief cleanup
     * The sql_queries table is kept, since it should persist between sessions.
     */
    ~SQLTab();

private:
    Ui::SQLTab *ui;

    void setSqlQuery(qlonglong id);
    void setSqlQuery(QString name);

private slots:
    void on_sqlBufferList_activated(const QModelIndex &index);
    void on_sqlLoad_clicked();
    void on_sqlSave_clicked();
    void on_sqlOutReload_clicked();
    void on_sqlPrint_clicked();
    void printTable(QPrinter* printer);
    void on_sqlBufferDeleteButton_clicked();
};

#endif // SQLTAB_H

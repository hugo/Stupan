#include "acquisitions.h"
#include "ui_acquisitions.h"

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlTableModel>
#include <QDebug>
#include <QSqlError>
#include <QTableWidgetItem>
#include <QVariant>
#include <QMessageBox>

#include <algorithm>

#include "macros.h"
#include "utils.h"
#include "moneydelegate.h"
#include "validatordelegate.h"
#include "comboboxitemdelegate.h"
#include "enums.h"
#include "db_enum.h"

namespace purchase {
enum purch_column {
    id = 0,
    name,
    amount,
    price,
    total_price,
};
}

Acquisitions::Acquisitions(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Acquisitions)
{
    ui->setupUi(this);

    MoneyDelegate* monDel = new MoneyDelegate(this);
    QIntValidator* anyIntVal = new QIntValidator(INT_MIN, INT_MAX, this);
    ValidatorDelegate* valDel = new ValidatorDelegate(anyIntVal, this);
    ComboBoxItemDelegate* cbiDel = new ComboBoxItemDelegate({{ SaleStatus::for_sale,     tr("Till salu") },
                                                             { SaleStatus::not_for_sale, tr("Ej till salu") },
                                                             { SaleStatus::hidden,       tr("Dold") }}, this);


    QSqlTableModel* listModel = new QSqlTableModel(this);
    listModel->setTable("products");
    listModel->setFilter(QString("sale_status IN (%1, %2)")
                         .arg(SaleStatus::for_sale)
                         .arg(SaleStatus::not_for_sale));
    listModel->setSort(DB::Products::Price, Qt::AscendingOrder);
    SET_HEADER_DATA(listModel,
            { DB::Products::Name,       tr("Namn") },
            { DB::Products::Price,      tr("Försäljningspris") },
            { DB::Products::SaleStatus, tr("Försäljningsstatus") });
    listModel->select();

    ui->acquistionProductView->setModel(listModel);

    HIDE_COLUMNS(ui->acquistionProductView,
                 DB::Products::Id,
                 DB::Products::BarCode,
                 DB::Products::BuyId,
                 DB::Products::SortingId);

    SET_COL_DELEGATORS(ui->acquistionProductView,
            { DB::Products::Price,      monDel },
            { DB::Products::SaleStatus, cbiDel /* NOTE this is technically overkill, since this field ins't editable */ });

    ui->acquistionProductView->header()->setSectionResizeMode(QHeaderView::ResizeToContents);
    ui->acquistionProductView->header()->setSectionResizeMode(DB::Products::Name, QHeaderView::Stretch);
    ui->acquistionProductView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    {
        /* Column width are handled by resizeToContents.
         * Actually allowing name to grow, and remaining to shrink
         * automatically would be slightly nicer (since the column width
         * wouldn't jump around as much. But this is good enough).
         *
         * Disabling of editing is handled manually, where each new QTableWidgetItem
         * is manually marked as non-editable (where applicable).
         */

        auto* v = ui->acquisitionPurchaseView;
        v->hideColumn(purchase::id);
        v->setItemDelegateForColumn(purchase::amount, valDel);
        v->setItemDelegateForColumn(purchase::price, monDel);
        v->setItemDelegateForColumn(purchase::total_price, monDel);

        QObject::connect(v, &QTableWidget::cellChanged,
                         this, &Acquisitions::recalculate_acquisition_row);
    }

    util::populateMoneyComboBox(ui->acquisitionAccountCombo);

    // We usually want the "Vault" account. So we select it by default.
    /* TODO actually select vault (incorrect magic number) */
    ui->acquisitionAccountCombo->setCurrentIndex(1);
}

Acquisitions::~Acquisitions()
{
    delete ui;
}

void Acquisitions::recalculate_acquisition_row(int row, int col) {

    // only the amount and price should be editable anyways
    if (!(col == purchase::amount || col == purchase::price)) return;

    auto* table = ui->acquisitionPurchaseView;

    auto amountCell = table->item(row, purchase::amount);
    auto priceCell = table->item(row, purchase::price);
    if (amountCell == nullptr || priceCell == nullptr) return;

    long long amount = amountCell->data(Qt::DisplayRole).toLongLong();
    long long price = priceCell->data(Qt::DisplayRole).toLongLong();

    long long total = amount * price;
    /* NOT translatable, since the display is handled by the money delegate */
    auto total_cell = new QTableWidgetItem(QString("%1").arg(total));
    total_cell->setFlags(total_cell->flags() & ~Qt::ItemIsEnabled);
    table->setItem(row, purchase::total_price, total_cell);

    {
        long long total = 0;
        for (int row = 0; row < ui->acquisitionPurchaseView->rowCount(); row++) {
            auto totalCell = table->item(row, purchase::total_price);
            if (! totalCell) continue;
            total += totalCell->data(Qt::DisplayRole).toLongLong();
        }
        ui->acquistionTotalPriceLabel->setText(tr("%L1", "Totalsumma för transaktion").arg(static_cast<double>(total) / 100.0));
    }
}


void Acquisitions::on_acquisitionAddButton_clicked()
{
    QItemSelectionModel* selectionModel = ui->acquistionProductView->selectionModel();
    QAbstractItemModel* sourceModel = ui->acquistionProductView->model();
    auto table = ui->acquisitionPurchaseView;

    /* Disabling sorting while adding columns.
     * Otherwise the newly added column will be sorted prematurely, making
     * us lose track of it.
     *
     * Possibly have some form of defer to ensure it's enabled on exit.
     */
    table->setSortingEnabled(false);
    foreach (QModelIndex index, selectionModel->selectedRows(DB::Products::Id)) {
        int row_idx = table->rowCount();

        /* Ensure that no duplicate gets added */
        for (int row = 0; row < row_idx; row++) {
            auto idCell = table->item(row, purchase::id);
            if (idCell == nullptr) continue;
            sqlite_key id = idCell->data(Qt::DisplayRole).toLongLong();
            if (id == index.data().toLongLong()) {
                table->selectRow(row);
                table->setSortingEnabled(true);
                return;
            }
        }

        QString name = sourceModel
                ->index(index.row(), DB::Products::Name)
                .data().toString();

        table->insertRow(row_idx);
        auto id_cell = new QTableWidgetItem(index.data().toString());
        id_cell->setFlags(id_cell->flags() & ~Qt::ItemIsEnabled);
        table->setItem(row_idx, purchase::id, id_cell);

        auto name_cell = new QTableWidgetItem(name);
        name_cell->setFlags(name_cell->flags() & ~Qt::ItemIsEnabled);
        table->setItem(row_idx, purchase::name, name_cell);

        table->setItem(row_idx, purchase::amount, new QTableWidgetItem("0"));

        QVariant id = index.data();
        QSqlQuery price_query;
        price_query.prepare("SELECT item_price FROM acquisitions "
                            "WHERE product_id = :id "
                            "ORDER BY time DESC "
                            "LIMIT 1");
        price_query.bindValue(":id", id);
        price_query.exec();
        QTableWidgetItem* cell;
        if (price_query.next()) {
            cell = new QTableWidgetItem(price_query.value(0).toString());
        } else {
            cell = new QTableWidgetItem("0");
        }
        table->setItem(row_idx, purchase::price, cell);
        table->selectRow(row_idx);
    }
    table->setSortingEnabled(true);
    table->resizeColumnsToContents();


}

void Acquisitions::on_acquisitionSearch_textChanged(const QString &arg1)
{
    bool isNumber;
    arg1.toInt(&isNumber, 10);
    QString filter = QString(
                isNumber
                ? "bar_code = '%1'"
                : "name LIKE '%' || '%1' || '%' AND sale_status IN (0, 1)"
                ).arg(arg1);

    QSqlTableModel* model = static_cast<QSqlTableModel*>(ui->acquistionProductView->model());
    model->setFilter(filter);
    model->select();
}

void Acquisitions::on_acquisitionSubmitButton_clicked()
{
    QVariant activeAccount = ui->acquisitionAccountCombo->currentData();

    QSqlQuery query;
    query.prepare("INSERT INTO acquisitions (product_id, item_price, amount, account) "
                  "VALUES (?, ?, ?, ?)");
    auto* table = ui->acquisitionPurchaseView;

    QVariantList ids, prices, amounts, accounts;
    for (int row = 0; row < table->rowCount(); row++) {
        ids      << table->item(row, purchase::id)->data(Qt::DisplayRole);
        prices   << table->item(row, purchase::price)->data(Qt::DisplayRole);
        amounts  << table->item(row, purchase::amount)->data(Qt::DisplayRole);
        accounts << activeAccount;
    }
    query.addBindValue(ids);
    query.addBindValue(prices);
    query.addBindValue(amounts);
    query.addBindValue(accounts);

    if (!query.execBatch()) {
        qDebug() << __FILE__ << __LINE__ << query.lastError();
        QMessageBox::warning(
                    this, tr("Inmatningen misslyckades"),
                    tr("Misslyckades med att lägga till inköpet i databasen.\n\nSQLFel: %1")
                    .arg(query.lastError().text()));
    }

    /* clear table */
    while (table->rowCount())
        table->removeRow(0);


    emit accounts_changed();
    emit open_till();
}

void Acquisitions::on_acquisitionDeleteButton_clicked()
{
    /* Removes from last to first row, since otherwise row indexes
     * would update with each remove */
    auto* table = ui->acquisitionPurchaseView;
    table->setSortingEnabled(false);
    QSet<int> toDeleteS;
    foreach (auto* item, table->selectedItems()) {
        toDeleteS.insert(item->row());
    }
    QList<int> toDelete = toDeleteS.values();
    std::sort(toDelete.begin(), toDelete.end());
    for (auto it = toDelete.rbegin(); it != toDelete.rend(); it++) {
        table->removeRow(*it);
    }
    table->setSortingEnabled(true);
}

void Acquisitions::update_products() {
    static_cast<QSqlTableModel*>(ui->acquistionProductView->model())->select();
#if 0
    QSqlQuery query;
    QVector<sqlite_key> ids;
    for (int row = 0; row < ui->acquisitionPurchaseView->rowCount(); i++) {
        auto item = ui->acquisitionPurchaseView->item(row, purchase::id);
        if (item == nullptr)
            ids << 0;
        else
            ids << item->data(Qt::DisplayRole).toLongLong();
    }
    auto qs = repeat("?", ids.len).join(",")
    query.prepare(QString("SELECT name FROM products WHERE id IN (%1)").arg(qs));
    for (sqlite_key id : ids) {
        query.bindValue(id);
    }
    if (! query.exec()) return;
    while (query.next()) {
        ui->acquisitionPurchaseView->setItem()
    }
#endif
}

void Acquisitions::reset_search() {
    ui->acquisitionSearch->setText("");
}

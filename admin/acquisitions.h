#ifndef ACQUISITIONS_H
#define ACQUISITIONS_H

#include <QWidget>
#include <QComboBox>

#include "disablingmodel.h"

namespace Ui {
class Acquisitions;
}

/**
 * @brief Controller for the Acquisitions tab
 *
 * This tab is for for adding items to the stock.
 */
class Acquisitions : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief Sets up the widget with the given Qt parent for cleanup.
     * @param parent Qt parent.
     */
    explicit Acquisitions(QWidget *parent = nullptr);
    /**
     * @brief Cleanup
     */
    ~Acquisitions();


public slots:
    /**
     * @brief Reload product from the database.
     *
     * Forces an update of the available products in the search view,
     * along with all entered names.
	 *
	 * @todo this should also include the purchase table
     */
    void update_products();

    /**
     * @brief reset_search
     *
     * Resets the search query to include everything.
     */
    void reset_search();

    /**
     * @brief Update the label declaring the total sum
     *
     * Recalculates the total sum of the added products,
     * and updates the label to display the new total.
     *
     * This is designed to be connected to a model change signal.
     * The parameters are checked so this becomes a no-op if a field
     * which doesn't affect the total is changed.
     *
     * @param row Row which has been updated.
     * @param col Column which has been updated.
     */
    void recalculate_acquisition_row(int row, int col);


signals:
    /**
     * @brief A request to open the cash drawer till
     */
    void open_till();

    /**
     * @brief One or more money accounts have changed their balances.
     */
    void accounts_changed();

private:
    Ui::Acquisitions *ui;

    void populateMoneyComboBox(QComboBox* box);
    void updateMoneyString();

private slots:
    void on_acquisitionAddButton_clicked();
    void on_acquisitionSearch_textChanged(const QString &arg1);
    void on_acquisitionSubmitButton_clicked();
    void on_acquisitionDeleteButton_clicked();

};

#endif // ACQUISITIONS_H

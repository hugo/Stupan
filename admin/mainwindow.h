#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "configuration.h"
#include "cashdrawer.h"

namespace Ui {
class MainWindow;
}

/**
 * @brief Actual Admin window
 *
 * Window, which just loads its tabs, and connects signals between them.
 *
 * (and also contains an open-till button)
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * @brief Sets up the window, and connects all childrens signals.
     * @param conf Configuration to use
     * @param parent For Qt cleanup.
     */
    explicit MainWindow(const Configuration &conf, QWidget *parent = 0);

    /**
     * Cleans up
     */
    ~MainWindow();

signals:
    /**
     * @brief A request to open the cash till.
     */
    void open_till();

    /**
     * @brief Something has changed with a money account.
     */
    void accounts_changed();

    /**
     * @brief Something has changed with the product database.
     *
     * This can include a products name, price, or anything else.
     */
    void products_changed();

private slots:
    void on_openTillButton_clicked();

private:
    Ui::MainWindow *ui;
    CashDrawer* drawer;
};

#endif // MAINWINDOW_H

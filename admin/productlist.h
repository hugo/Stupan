#ifndef PRODUCTLIST_H
#define PRODUCTLIST_H

#include <QWidget>

#include <QSqlTableModel>

namespace Ui {
class ProductList;
}

/**
 * @brief Controller for the list of products.
 */
class ProductList : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief Sets up the widget.
     * Sets up the database view products_with_stock,
     * along with a trigger for propagating changes to the products table.
     * @param parent For Qt cleanup
     */
    explicit ProductList(QWidget *parent = nullptr);
    /**
     * @brief cleanup
     */
    ~ProductList();

private:
    Ui::ProductList *ui;
    QSqlTableModel* model;
    void setProductFilter();

signals:
    /**
     * @brief One or more products changed.
     */
    void products_changed();

    /**
     * @brief A new product was added to the system.
     */
    void new_product();

private slots:
    void on_productListShowHiddenCheckBox_toggled(bool checked);
    void on_productListNewButton_clicked();
    void on_productListDeleteButton_clicked();
    void on_productListSearch_textChanged(const QString &arg1);
};

#endif // PRODUCTLIST_H

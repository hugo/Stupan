-- NOTE: this only makes sense if the whole table is submitted at once
CREATE TEMPORARY TRIGGER update_view_trigger
INSTEAD OF UPDATE ON diff_view
FOR EACH ROW
WHEN NEW.actual IS NOT NULL
BEGIN
    INSERT INTO stock_diff (product_id, expected, actual)
    VALUES (NEW.product_id, NEW.expected, NEW.actual);
END

#include "stockcheck.h"
#include "ui_stockcheck.h"

#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>

#include "disablingmodel.h"
#include "macros.h"
#include "qintornullvalidator.h"
#include "maybevalidatordelegate.h"
#include "utils.h"
#include "db_enum.h"
#include "enums.h"

StockCheck::StockCheck(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StockCheck)
{
    ui->setupUi(this);

    clear_stockDiffTemp();

    /* Everything except Actual */
    DisablingModel* inventoryModel = new DisablingModel(
                { DB::DiffView::ProductId,
                  DB::DiffView::SaleStatus,
                  DB::DiffView::Name,
                  DB::DiffView::Expected,
                  DB::DiffView::Diff }, this);
    inventoryModel->setTable("diff_view");

    SET_HEADER_DATA(inventoryModel,
            { DB::DiffView::ProductId,  tr("id")},
            { DB::DiffView::SaleStatus, tr("sale status")},
            { DB::DiffView::Name,       tr("Namn")},
            { DB::DiffView::Expected,   tr("Förväntat")},
            { DB::DiffView::Actual,     tr("Faktiskt")},
            { DB::DiffView::Diff,       tr("Diff")});

    inventoryModel->setEditStrategy(QSqlTableModel::OnManualSubmit);

    QObject::connect(inventoryModel, &QAbstractItemModel::dataChanged,
                     this, &StockCheck::recalculate_diff_column);

    inventoryModel->select();
    ui->inventoryCheckView->setModel(inventoryModel);
    ui->inventoryCheckView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    ui->inventoryCheckView->setEditTriggers(QAbstractItemView::AllEditTriggers);
    QIntOrNullValidator* intOrNullVal = new QIntOrNullValidator(INT_MIN, INT_MAX, this);
    MaybeValidatorDelegate* maybeValDel = new MaybeValidatorDelegate(intOrNullVal, this);
    ui->inventoryCheckView->setItemDelegateForColumn(4, maybeValDel);

    HIDE_COLUMNS(ui->inventoryCheckView,
                 DB::DiffView::ProductId,
                 DB::DiffView::SaleStatus);

    ui->inventoryQuickInventoryCheckBox->setChecked(true);
}

StockCheck::~StockCheck()
{
    delete ui;
}

void StockCheck::recalculate_diff_column(
        const QModelIndex &topLeft,
        const QModelIndex& /*bottomRight*/,
        const QVector<int>& /*roles*/
) {
    QSqlTableModel* model = static_cast<QSqlTableModel*>(ui->inventoryCheckView->model());

    if (model->isDirty()) {
        int row = topLeft.row();

        bool number;
        int expected = model->data(model->index(row, DB::DiffView::Expected)).toInt();
        int actual = model->data(model->index(row, DB::DiffView::Actual)).toInt(&number);

        model->setData(model->index(row, DB::DiffView::Diff),
                       number ? actual - expected
                              : QVariant());
    }
}

/*
 * Calculates the total price in the acquisitions menu
 */


void StockCheck::on_inventoryStockDiffSubmit_clicked()
{
    static_cast<QSqlRelationalTableModel*>(ui->inventoryCheckView->model())->submitAll();
}

void StockCheck::on_inventoryStockDiffReset_clicked()
{
    auto* model = static_cast<QSqlRelationalTableModel*>(ui->inventoryCheckView->model());
    /* revertAll fixes the data, but NOT the product names */
    model->revertAll();
    model->select();
}

/*
 * Note that the hidden rows can still contain data that WILL be
 * submitted.
 * TODO Possibly have some form of warning when it comes to that.
 */
void StockCheck::on_inventoryQuickInventoryCheckBox_toggled(bool checked)
{
    static_cast<QSqlRelationalTableModel*>(ui->inventoryCheckView->model())
            ->setFilter( checked
                         ? QString("sale_status = %1 or expected != 0").arg(SaleStatus::for_sale)
                         : QString("sale_status in (%1, %2)")
                           .arg(SaleStatus::for_sale)
                           .arg(SaleStatus::not_for_sale));
}



/* TODO rename this function to better reflect what it actually does
 * (recreating all tables needed for stock diff)
 */
void StockCheck::clear_stockDiffTemp()
{
    util::runSql("DROP VIEW IF EXISTS diff_view");
    util::runSqlResource(":/stockcheck/view/diff_view.sql");
    util::runSqlResource(":/stockcheck/trigger/update_view_trigger.sql");
}

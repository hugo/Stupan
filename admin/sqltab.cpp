#include "sqltab.h"
#include "ui_sqltab.h"

#include <QSqlQueryModel>
#include <QSqlTableModel>
#include <QSqlQuery>
#include <QMessageBox>
#include <QPrintPreviewDialog>
#include <QSqlError>
#include <QDebug>
#include <QPainter>

#include "tableprinter.h"

#include "macros.h"
#include "utils.h"
#include "db_enum.h"

SQLTab::SQLTab(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SQLTab)
{
    ui->setupUi(this);

    QSqlQueryModel* sqlQueryModel = new QSqlQueryModel(this);
    ui->sqlOutput->setModel(sqlQueryModel);

    util::runSqlResource(":/sqltab/tables/sql_queries.sql");

    QSqlTableModel* sqlQueryListModel = new QSqlTableModel(this);
    sqlQueryListModel->setTable("sql_queries");
    sqlQueryListModel->select();
    SET_HEADER_DATA(sqlQueryListModel,
            { DB::SqlQueries::Id,    tr("id") },
            { DB::SqlQueries::Name,  tr("Namn") },
            { DB::SqlQueries::Query, tr("SQL Fråga") });
    ui->sqlBufferList->setModel(sqlQueryListModel);
    HIDE_COLUMNS(ui->sqlBufferList, DB::SqlQueries::Id);
}

SQLTab::~SQLTab()
{
    delete ui;
}

void SQLTab::on_sqlBufferList_activated(const QModelIndex &index)
{
    QAbstractItemModel* model = ui->sqlBufferList->model();
    qlonglong id = model->data(model->index(index.row(), DB::SqlQueries::Id))
            .toLongLong();
    setSqlQuery(id);
}

void SQLTab::on_sqlLoad_clicked()
{
    setSqlQuery(ui->sqlBufferName->text());
}

void SQLTab::on_sqlSave_clicked()
{
    QSqlQuery query;
    query.prepare("INSERT OR REPLACE "
                  "INTO sql_queries (name, query) "
                  "VALUES (:name, :query)");
    query.bindValue(":name", ui->sqlBufferName->text());
    query.bindValue(":query", ui->sqlEdit->toPlainText());
    query.exec();

    static_cast<QSqlTableModel*>(ui->sqlBufferList->model())->select();
}

void SQLTab::on_sqlOutReload_clicked()
{
    QSqlQueryModel* model = static_cast<QSqlQueryModel*>(ui->sqlOutput->model());
    model->setQuery(ui->sqlEdit->toPlainText());

    if (model->lastError().isValid()) {
        QMessageBox::warning(
                    this, tr("SQLmisslyckande"),
                    tr("Misslyckades att ställa förfrågan") + "\n\n " + model->lastError().text());
        return;
    }

    ui->sqlDataContainer->setCurrentIndex(0);
}

void SQLTab::printTable(QPrinter* printer)
{
    QPainter painter;
    if (!painter.begin(printer)) {
        qWarning() << tr("Kan inte starta skrivare");
        return;
    }

    QSqlQueryModel* model = static_cast<QSqlQueryModel*>(ui->sqlOutput->model());

    TablePrinter tablePrinter(&painter, printer);

    QVector<int> columnStretch = QVector<int>();
    QStringList headers;
    for (int i = 0; i <  model->columnCount(); i++) {
        columnStretch << 1;
        headers << model->headerData(i, Qt::Horizontal).toString();
    }

    QFont headerFont;
    headerFont.setBold(true);
    tablePrinter.setHeadersFont(headerFont);

    if (!tablePrinter.printTable(ui->sqlOutput->model(), columnStretch, headers.toVector())) {
        qDebug() << tablePrinter.lastError();
    }

    painter.end();
}

void SQLTab::on_sqlPrint_clicked()
{
    QPrintPreviewDialog dialog;
    QObject::connect(&dialog, &QPrintPreviewDialog::paintRequested,
                     this, &SQLTab::printTable);
    dialog.exec();
}

void SQLTab::on_sqlBufferDeleteButton_clicked()
{
    QSqlTableModel* model = static_cast<QSqlTableModel*>(ui->sqlBufferList->model());

    QSqlQuery query;
    query.prepare("DELETE FROM sql_queries WHERE id = ?");

    QVariantList ids;
    foreach (QModelIndex index,  ui->sqlBufferList->selectionModel()->selectedRows())
    {
        ids << model->data(model->index(index.row(), DB::SqlQueries::Id));
    }
    query.addBindValue(ids);

    if (!query.execBatch())
        qDebug() << query.lastError();

    model->select();
}


void SQLTab::setSqlQuery(qlonglong id)
{
    QSqlQuery query;
    query.prepare("SELECT name, query FROM sql_queries "
                  "WHERE id = :id");
    query.bindValue(":id", id);
    query.exec();

    if (query.next()) {
        ui->sqlBufferName->setText(query.value(0).toString());
        ui->sqlEdit->setPlainText(query.value(1).toString());
    }
}

void SQLTab::setSqlQuery(QString name)
{
    QSqlQuery query;
    query.prepare("SELECT name, query FROM sql_queries "
                  "WHERE name = :name");
    query.bindValue(":name", name);
    query.exec();

    if (query.next()) {
        ui->sqlBufferName->setText(query.value(0).toString());
        ui->sqlEdit->setPlainText(query.value(1).toString());
    }
}

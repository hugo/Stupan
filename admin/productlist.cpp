#include "productlist.h"
#include "ui_productlist.h"

#include <QSqlQuery>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QSqlError>

#include "disablingmodel.h"
#include "validatordelegate.h"
#include "comboboxitemdelegate.h"
#include "moneydelegate.h"
#include "macros.h"
#include "enums.h"
#include "utils.h"
#include "db_enum.h"

ProductList::ProductList(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ProductList)
{
    ui->setupUi(this);

    util::runSql("DROP VIEW IF EXISTS products_with_stock.sql");
    util::runSqlResource(":/productlist/view/products_with_stock.sql");
    util::runSqlResource(":/productlist/trigger/propagate_products_with_stock.sql");

    model = new DisablingModel({DB::ProductsWithStock::Id,
                                DB::ProductsWithStock::Total}, this);
    model->setTable("products_with_stock");

    model->setEditStrategy(QSqlTableModel::OnFieldChange);

    model->select();
    SET_HEADER_DATA(model,
            { DB::ProductsWithStock::Id,         tr("id")},
            { DB::ProductsWithStock::SortingId,  tr("Sorterings-ID")},
            { DB::ProductsWithStock::BuyId,      tr("Systembolaget ID")},
            { DB::ProductsWithStock::BarCode,    tr("Streckkod")},
            { DB::ProductsWithStock::Name,       tr("Namn")},
            { DB::ProductsWithStock::Price,      tr("Pris")},
            { DB::ProductsWithStock::SaleStatus, tr("Till Salu?")},
            { DB::ProductsWithStock::Total,      tr("Lager")});

    QObject::connect(
                model, &QSqlTableModel::dataChanged,
                this, [this](const QModelIndex&, const QModelIndex&, const QVector<int>&) {
        emit products_changed();
    });

    QTableView* view = ui->productListTableView;
    view->setModel(model);
    HIDE_COLUMNS(view,
                 DB::ProductsWithStock::Id,
                 DB::ProductsWithStock::BuyId,
                 DB::ProductsWithStock::BarCode
                 );
    view->setEditTriggers(QAbstractItemView::AllEditTriggers);

    QIntValidator* anyIntVal = new QIntValidator(INT_MIN, INT_MAX, this);
    ValidatorDelegate* valDel = new ValidatorDelegate(anyIntVal, this);
    auto cbiDel = new ComboBoxItemDelegate(
                {{ SaleStatus::for_sale,     tr("Till salu") },
                 { SaleStatus::not_for_sale, tr("Ej till salu") },
                 { SaleStatus::hidden,       tr("Dold") }}, this);

    SET_COL_DELEGATORS(view,
            { DB::ProductsWithStock::SortingId, valDel },
            { DB::ProductsWithStock::BuyId, valDel },
            /* NOTE we pretend that "cents" doesn't exist here, even
             * though they do. This mainly means that if a products
             * sell price contains "cents" then those won't be shown,
             * leading to people paying less than they should.
             * This is however not a problem. */
            { DB::ProductsWithStock::Price, new MoneyDelegate(this, 0) },
            { DB::ProductsWithStock::SaleStatus, cbiDel });

    view->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    view->horizontalHeader()->setSectionResizeMode(DB::ProductsWithStock::Name,
                                                   QHeaderView::Stretch);

    setProductFilter();
}

ProductList::~ProductList()
{
    delete ui;
}

void ProductList::on_productListShowHiddenCheckBox_toggled(bool /*checked*/)
{
    setProductFilter();
}

// Add new row for new product
void ProductList::on_productListNewButton_clicked()
{
    // trigger creates stock entry
    QSqlQuery("INSERT INTO products DEFAULT VALUES");
    model->select();

    emit new_product();

}

// TODO can't remove item which is used elsewhere,
//      currently fails silently.
void ProductList::on_productListDeleteButton_clicked()
{
    QItemSelectionModel* selection = ui->productListTableView->selectionModel();
    if (!selection->hasSelection()) return;
    QModelIndexList lst = selection->selectedRows();
    foreach ( QModelIndex index, lst ) {
        model->removeRow(index.row());
    }
}

void ProductList::on_productListSearch_textChanged(const QString&)
{
    setProductFilter();
}

void ProductList::setProductFilter()
{
    QString searchStr = ui->productListSearch->text();

    bool isNumber;
    searchStr.toInt(&isNumber, 10);

    QString filter;

    if (isNumber) {
        /* A barcode was scanned, show product even if hidden */
        filter = QString("bar_code = '%1'").arg(searchStr);
    } else {
        /* A free-text search was made, honor the hidden flag */

        /* TODO injection attack */
        filter = QString("name LIKE '%' || '%1' || '%'").arg(searchStr);

        if (! ui->productListShowHiddenCheckBox->isChecked()) {
            filter.append(QString(" AND sale_status IN (%1, %2)")
                          .arg(SaleStatus::for_sale)
                          .arg(SaleStatus::not_for_sale));
        }
        filter.append(QString(" AND sale_status != %1")
                      .arg(SaleStatus::system));
    }

    /* TODO magic string */
    filter.append(" OR name = 'CHANGE ME'");

    QSqlTableModel* model = static_cast<QSqlTableModel*>(ui->productListTableView->model());
    model->setFilter(filter);
    model->select();
}

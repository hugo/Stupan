#include "drainage.h"
#include "ui_drainage.h"

#include <QSqlQueryModel>
#include <QSqlQuery>
#include <QDateTimeEdit>
#include <QDebug>
#include <QSqlError>
#include <QMessageBox>

#include "macros.h"
#include "moneydelegate.h"

Drainage::Drainage(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Drainage)
{
    ui->setupUi(this);

    MoneyDelegate* monDel = new MoneyDelegate(this);

    // TODO some of these things possibly should requery under usage
    // Autoupdate, as it's called

    QSqlQueryModel* drainageModel = new QSqlQueryModel(this);
    drainageModel->setQuery("SELECT id, "
                            "    datetime(start_time, 'localtime'), "
                            "    datetime(end_time, 'localtime'), "
                            "    decrease,"
                            "    how_often_minutes, "
                            "    min_price "
                            "FROM drainage "
                            "WHERE strftime('%s', start_time) > strftime('%s', 'now') "
                            "ORDER BY start_time");
    if (drainageModel->lastError().isValid()) {
        qDebug() << __FILE__ << __LINE__ << drainageModel->lastError();
    }
    SET_HEADER_DATA(drainageModel,
                    { 1, tr("Starttid") },
                    { 2, tr("Sluttid") },
                    { 3, tr("Kr/Tid") },
                    { 4, tr("Hur ofta") },
                    { 5, tr("Minimipris") }
                    );
    ui->drainageFutureView->setModel(drainageModel);
    HIDE_COLUMNS(ui->drainageFutureView, 0);
    SET_COL_DELEGATORS(ui->drainageFutureView,
            { 3, monDel },
            { 5, monDel });
    ui->drainageFutureView->header()->setSectionResizeMode(QHeaderView::ResizeToContents);

    QDateTime now = QDateTime::currentDateTime();
    //ui->drainageStartTimeDateTimeEdit->setMinimumDateTime(now);
    ui->drainageEndTimeDateTimeEdit->setMinimumDateTime(now);
    ui->drainageStartTimeDateTimeEdit->setDateTime(now);
    ui->drainageEndTimeDateTimeEdit->setDateTime(now);

    updateCurrentDrainageStatus();

    /* PAST DRAINAGES */

    // TODO show how much was sold during a drainage, and how much we lost during it

    QSqlQueryModel* pastDrainageModel = new QSqlQueryModel(this);
    pastDrainageModel->setQuery("SELECT id, "
                                "   datetime(start_time, 'localtime'), "
                                "   datetime(end_time, 'localtime'), "
                                "   decrease, "
                                "   how_often_minutes, "
                                "   min_price "
                                "FROM drainage "
                                "WHERE strftime('%s', end_time) < strftime('%s', 'now') "
                                "ORDER BY start_time");
    ui->drainagePastView->setModel(pastDrainageModel);
    SET_HEADER_DATA(pastDrainageModel,
                    { 1, tr("Starttid") },
                    { 2, tr("Sluttid") },
                    { 3, tr("Kr/Tid") },
                    { 4, tr("Hur ofta") },
                    { 5, tr("Minimipris") }
                    );
    HIDE_COLUMNS(ui->drainagePastView, 0);
    SET_COL_DELEGATORS(ui->drainagePastView,
            { 3, monDel },
            { 5, monDel });
    ui->drainagePastView->header()->setSectionResizeMode(QHeaderView::ResizeToContents);

}

Drainage::~Drainage()
{
    delete ui;
}

void Drainage::on_drainageAddButton_clicked()
{
    // Products to go from crown to öre
    QVariant per_hour = ui->drainageAmountPerHourSpinBox->value() * 100;
    QVariant min_price = ui->drainageMinPriceSpinBox->value() * 100;
    QVariant how_often = ui->drainageHowOftenSpinBox->value();

    QDateTime start_time = ui->drainageStartTimeDateTimeEdit->dateTime().toUTC();
    QDateTime end_time = ui->drainageEndTimeDateTimeEdit->dateTime().toUTC();

    /*
    if (end_time < QDateTime::currentDateTime().toUTC()) {
        QMessageBox::warning(this, "Error", "Kan inte skapa tömmning helt i det förflutna");
        return;
    }

    // This shouldn't be able to happen due to the end_time min being set to start_time
    if (start_time > end_time) {
        QMessageBox::warning(this, "Error", "Jag går inte med på krökningar i rumtiden!");
        return;
    }
    */

    // drainages always start and end on a whole minute
    QString timeStr = "yyyy-MM-dd hh:mm:00";
    QVariant start_str = start_time.toString(timeStr);
    QVariant end_str = end_time.toString(timeStr);

    QSqlQuery query;
    query.prepare ("INSERT INTO drainage (start_time, end_time, decrease, how_often_minutes, min_price) "
                   "VALUES (:start, :end, :dec, :often, :min)");
    query.bindValue(":start", start_str);
    query.bindValue(":end", end_str);
    query.bindValue(":min", min_price);
    query.bindValue(":dec", per_hour);
    query.bindValue(":often", how_often);

    if (!query.exec()) {
        qDebug() << __FILE__ << __LINE__ << query.lastError();
        // Drainages in the past, overlapping, or with negative time spans
        QMessageBox::warning(this, "Error", query.lastError().databaseText());
    }

    updateDrainageView();
    updateCurrentDrainageStatus();

}

void Drainage::updateDrainageView()
{
    QSqlQueryModel* model = static_cast<QSqlQueryModel*>(ui->drainageFutureView->model());
    QString queryStr = model->query().executedQuery();
    model->clear();
    model->setQuery(queryStr);
}

void Drainage::on_drainageTimeZoneComboBox_currentIndexChanged(int index)
{
    /* NOTE
	 * The 0 here is for LocalTime. It *might* be possible to instead
	 * trigger on currentTextChanged, that depends on if we get the
	 * translated or untranslated version of the string as argument.
	 * TODO test this
	 */
    Qt::TimeSpec spec = index == 0 ? Qt::LocalTime : Qt::UTC;
    QDateTimeEdit* startEdit = ui->drainageStartTimeDateTimeEdit;
    QDateTimeEdit* endEdit   = ui->drainageEndTimeDateTimeEdit;

    startEdit->setTimeSpec(spec);
    endEdit->setTimeSpec(spec);

    // This is to update the ui
    startEdit->setDateTime(startEdit->dateTime());
    endEdit->setDateTime(endEdit->dateTime());
}

void Drainage::on_drainageStartTimeDateTimeEdit_dateTimeChanged(const QDateTime &dateTime)
{
    ui->drainageEndTimeDateTimeEdit->setMinimumDateTime(dateTime);
}

#if 0
/* Having both these limiters "locks" the two fields to each other, meaning that end time needs to be
 * Set before start time can be set. The UI suffered more from this, than from being able to enter
 * invalid intervals (and getting an error prompt).
 */
void Drainage::on_drainageEndTimeDateTimeEdit_dateTimeChanged(const QDateTime &dateTime)
{
    ui->drainageStartTimeDateTimeEdit->setMaximumDateTime(dateTime);
}
#endif

void Drainage::on_drainageRemoveButton_clicked()
{
    QSqlQuery query;
    query.prepare("DELETE FROM drainage WHERE id = ?");
    foreach ( QModelIndex i, ui->drainageFutureView->selectionModel()->selectedRows(0) ) {
        QVariantList lst;
        lst << i.data();
        query.addBindValue(lst);
    }
    /* NOTE this will fail if nothing was selected. This can safely be ignored */
    if (!query.execBatch())
        qDebug() << __FILE__ << __LINE__ << query.lastError();
    updateDrainageView();
}

void Drainage::on_drainageCancelButton_clicked()
{
    QSqlQuery("UPDATE drainage SET end_time = CURRENT_TIMESTAMP "
              "WHERE CURRENT_TIMESTAMP BETWEEN start_time AND end_time");

    // TODO this should be done 1 secound later
    updateCurrentDrainageStatus();
}

void Drainage::on_drainageUpdateButton_clicked()
{
    updateCurrentDrainageStatus();
}

// TODO this should preferably also be called when a drainage starts
// due to the passage of time

void Drainage::updateCurrentDrainageStatus()
{
    // division to get crowns
    QSqlQuery currentDrainageQuery("SELECT datetime(start_time, 'localtime'), "
                                   "       datetime(end_time, 'localtime'), "
                                   "       decrease / 100.0, "
                                   "       how_often_minutes, "
                                   "       min_price / 100.0 "
                                   "FROM drainage "
                                   "WHERE CURRENT_TIMESTAMP BETWEEN start_time AND end_time "
                                   "LIMIT 1");

    if (currentDrainageQuery.next()) {
        QString startStr   = currentDrainageQuery.value(0).toString();
        QString endStr     = currentDrainageQuery.value(1).toString();
        double decreaseStr = currentDrainageQuery.value(2).toDouble();
        QString minutesStr = currentDrainageQuery.value(3).toString();
        double minStr      = currentDrainageQuery.value(4).toDouble();
        ui->drainageLabel->setText(tr("%1 kr/%2 min, ner till %3 kr\nMellan %4 och %5 lokal tid")
                                   .arg(decreaseStr, 0, 'f', 2)
                                   .arg(minutesStr)
                                   .arg(minStr, 0, 'f', 2)
                                   .arg(startStr)
                                   .arg(endStr));
        ui->drainageCancelButton->setVisible(true);
    } else {
        ui->drainageLabel->setText(tr("Ingen pågående tömning"));
        ui->drainageCancelButton->setVisible(false);
    }
}


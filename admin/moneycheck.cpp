#include "moneycheck.h"
#include "ui_moneycheck.h"
#include <QDebug>
#include <QSqlError>
#include <utility>
#include <vector>
#include "utils.h"

/*
 * NOTE: This code works in kronor instead of ören as the rest of the program.
 * This only works because sweden doesn't have any "fractional" cash.
 */


/*(
 * TODO
 * - Initial state
 * - submit
 */


/* TODO these should be configurable for i18n purposes */
static const std::vector<int> CASH_DENOMINATIONS
= {1, 2, 5, 10, 20, 50, 100, 200, 500, 1000};
/* First value is denomination, second is sum of one tube */
static const std::vector<std::pair<int, int>> COINTUBE_DENOMINATIONS
= {{1, 50}, {2, 100}, {5, 200}, {10, 250}};

MoneyCheck::MoneyCheck(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MoneyCheck)
{
    ui->setupUi(this);

    /* Setup cash counters */
    for (int i : CASH_DENOMINATIONS) {
        auto sb = findChild<QSpinBox*>(QString("l%1SpinBox").arg(i));
        QObject::connect(sb, QOverload<int>::of(&QSpinBox::valueChanged),
                         [=](int x) {
            this->findChild<QLabel*>(QString("s%1label").arg(i))->setText(QString("= %1").arg(x * i));
            ui->sumCash->setValue(this->sum_cash());
        });

        QObject::connect(sb, &QSpinBox::editingFinished,
                         [=](){
            if (sb->value() != 0) {
                ui->sumCash->setDisabled(true);
                ui->sumTotal->setDisabled(true);
            }
        });
    }

    /* Setup coin-tube counters */
    for (auto p : COINTUBE_DENOMINATIONS) {
        auto sb = findChild<QSpinBox*>(QString("t%1SpinBox").arg(p.first));
        QObject::connect(sb, QOverload<int>::of(&QSpinBox::valueChanged),
                         [=](int x) {
            this->findChild<QLabel*>(QString("t%1label")
                                     .arg(p.first))->setText(QString("= %1").arg(x * p.second));
            ui->sumCointube->setValue(this->sum_cointube());
        });

        QObject::connect(sb, &QSpinBox::editingFinished,
                         [=]() {
            if (sb->value() != 0) {
                ui->sumCointube->setDisabled(true);
                ui->sumTotal->setDisabled(true);
            }
        });
    }

    util::populateMoneyComboBox(ui->diffAccount);

    QObject::connect(this, &MoneyCheck::expectedChanged,
                     [=](int expected) { ui->expectedAmount->setText(tr("%1 kr").arg(expected)); });

    QObject::connect(this, &MoneyCheck::expectedChanged,
                     this, &MoneyCheck::updateDiff);
    QObject::connect(ui->sumTotal, QOverload<int>::of(&QSpinBox::valueChanged),
                     this, &MoneyCheck::updateDiff);

    QObject::connect(ui->resetAllButton, &QPushButton::clicked,
                     this, &MoneyCheck::resetAll);
    QObject::connect(ui->resetCointubeButton, &QPushButton::clicked,
                     this, &MoneyCheck::resetCointube);
    QObject::connect(ui->resetCashButton, &QPushButton::clicked,
                     this, &MoneyCheck::resetCash);

    QObject::connect(ui->diffAccount, &QComboBox::currentTextChanged,
                     this, &MoneyCheck::reloadExpected);

    this->loadStuff();

}

MoneyCheck::~MoneyCheck()
{
    delete ui;
}

void MoneyCheck::updateDiff() {
    long long int expected = this->expected;
    long long int actual = ui->sumTotal->value();
    long long int diff = actual - expected;

    QString msg;
    if (diff == 0) {
        msg = tr("Allt är som sig bör");
    } else if (diff < 0) {
        msg = tr("Det saknas %1 kr från kontot").arg(std::abs(diff));
    } else {
        msg = tr("Det är %1 kr mer än förväntat").arg(diff);
    }
    ui->resultMessage->setText(msg);
}

long long int MoneyCheck::sum_cash() {
    unsigned long long sum = 0;
    for (int i : CASH_DENOMINATIONS) {
        auto sb = findChild<QSpinBox*>(QString("l%1SpinBox").arg(i));
        sum += i * sb->value();
    }
    return sum;
}

long long int MoneyCheck::sum_cointube() {
    unsigned long long sum = 0;
    for (auto p : COINTUBE_DENOMINATIONS) {
        auto sb = findChild<QSpinBox*>(QString("t%1SpinBox").arg(p.first));
        sum += p.second * sb->value();
    }
    return sum;
}

void MoneyCheck::on_sumCash_valueChanged(int x)
{
    ui->sumTotal->setValue(x + ui->sumCointube->value());
}

void MoneyCheck::on_sumCointube_valueChanged(int x)
{
    ui->sumTotal->setValue(x + ui->sumCash->value());
}

void MoneyCheck::on_sumTotal_valueChanged(int x)
{
    ui->grandTotal->setText(tr("%1 kr").arg(x));
}

void MoneyCheck::on_sumCash_editingFinished()
{
    if (ui->sumCash->value() == 0) return;

    ui->sumTotal->setDisabled(true);
    for (int i : CASH_DENOMINATIONS) {
        findChild<QSpinBox*>(QString("l%1SpinBox").arg(i))->setDisabled(true);
    }
}


void MoneyCheck::on_sumCointube_editingFinished()
{
    if (ui->sumCointube->value() == 0) return;

    ui->sumTotal->setDisabled(true);

    for (auto p : COINTUBE_DENOMINATIONS) {
        findChild<QSpinBox*>(QString("t%1SpinBox").arg(p.first))->setDisabled(true);
    }
}


void MoneyCheck::on_sumTotal_editingFinished()
{
    if (ui->sumTotal->value() == 0) return;

    ui->sumCash->setDisabled(true);
    ui->sumCointube->setDisabled(true);

    for (auto p : COINTUBE_DENOMINATIONS) {
        findChild<QSpinBox*>(QString("t%1SpinBox").arg(p.first))->setDisabled(true);
    }

    for (int i : CASH_DENOMINATIONS) {
        findChild<QSpinBox*>(QString("l%1SpinBox").arg(i))->setDisabled(true);
    }
}


void MoneyCheck::resetCash()
{
    ui->sumCash->setDisabled(false);
    ui->sumCash->setValue(0);
    for (int i : CASH_DENOMINATIONS) {
        auto sb = findChild<QSpinBox*>(QString("l%1SpinBox").arg(i));
        sb->setDisabled(false);
        sb->setValue(0);
    }
    if (ui->sumCointube->value() == 0) {
        ui->sumTotal->setDisabled(false);
    }
}


void MoneyCheck::resetCointube()
{
    ui->sumCointube->setDisabled(false);
    ui->sumCointube->setValue(0);
    for (auto p : COINTUBE_DENOMINATIONS) {
        auto sb = findChild<QSpinBox*>(QString("t%1SpinBox").arg(p.first));
        sb->setDisabled(false);
        sb->setValue(0);
    }
    if (ui->sumCash->value() == 0) {
        ui->sumTotal->setDisabled(false);
    }
}

void MoneyCheck::resetAll()
{
    this->resetCash();
    this->resetCointube();
    ui->sumTotal->setDisabled(false);
    ui->sumTotal->setValue(0);
}

void MoneyCheck::loadStuff() {
    this->reloadExpected(ui->diffAccount->currentText());
}

void MoneyCheck::reloadExpected(const QString &arg1)
{
    QSqlQuery q;
    q.prepare("SELECT total "
              "FROM account_balances a "
              "LEFT JOIN money m "
              "ON a.account_id = m.id "
              "WHERE m.name = :name");
    q.bindValue(":name", arg1);
    q.exec();
    q.next();
    long long int expected = q.value(0).toLongLong() / 100;
    this->setExpected(expected);
}


void MoneyCheck::on_submitButton_clicked()
{
    // TODO this doesn't update the money menu

    long long int actual = ui->sumTotal->value() * 100;

    QSqlQuery query;
    query.prepare("INSERT INTO money_diffs (actual, expected, account) "
                  "SELECT :total, total, account_id "
                  "FROM account_balances a "
                  "LEFT JOIN money m "
                  "ON a.account_id = m.id "
                  "WHERE m.name = :name");
    query.bindValue(":total", actual);
    query.bindValue(":name", ui->diffAccount->currentText());
    if (!query.exec())
        qDebug() << __FILE__ << __LINE__ << query.lastError();

    this->resetAll();

    this->loadStuff();
}


void MoneyCheck::on_pushButton_clicked()
{
    this->loadStuff();
}

void MoneyCheck::updateMoney() {
    this->loadStuff();
}

CREATE TEMPORARY VIEW money_transfers_simple AS
SELECT
    t.time AS time,
    m.name AS from_acc,
    n.name AS to_acc,
    printf('%.2f', t.change / 100.0) AS amount,
    t.note AS note
FROM money_transfers t
LEFT JOIN money m ON t.from_acc = m.id
LEFT JOIN money n ON t.to_acc = n.id
ORDER by time DESC

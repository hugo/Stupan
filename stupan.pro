TEMPLATE = subdirs

SUBDIRS += \
    StupanCommon \
    admin/StupanAdmin.pro \
    kassa/StupanKassa.pro

# This is needed due to how the ui-files specify their icons
# Figure out something stabler (which especially doesn't depend on the
# PWD of the process), and then remove this.
icons.path = /usr/share/stupan/icons
icons.files = icons/*.png

pixmaps.path = /usr/share/pixmaps
pixmaps.files = icons/*.png

desktop.path = /usr/share/applications
desktop.files = *.desktop

INSTALLS += icons \
            pixmaps \
            desktop

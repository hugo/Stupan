PRAGMA FOREIGN_KEYS = ON; -- do this for all database connections

-- ============================== Tables =================================

-- Products entered into the system
-- Including their current sale price
CREATE TABLE products ( -- products available
    id INTEGER PRIMARY KEY NOT NULL,
    sorting_id INTEGER DEFAULT 0, -- for ui sorting
    buy_id INTEGER, -- Systembolagets id, om behov skulle finnas
    bar_code TEXT UNIQUE, -- för framtiden
    name TEXT DEFAULT 'CHANGE ME' NOT NULL,
    price INTEGER DEFAULT 0 NOT NULL,
    sale_status INTEGER DEFAULT 1
    -- see StupanCommon/enums.h for acceptable values
);

-- Log of all sold products
CREATE TABLE log (
    id INTEGER PRIMARY KEY NOT NULL,
    product_id INTEGER NOT NULL,
    time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    price INTEGER NOT NULL,
    account INTEGER NOT NULL REFERENCES money(id),

    FOREIGN KEY (product_id) REFERENCES products(id)
);

-- Possible types of a money account.
CREATE TABLE money_account_type (
    id INTEGER PRIMARY KEY NOT NULL,
    name TEXT UNQIUE NOT NULL
);

INSERT OR IGNORE INTO money_account_type (name) VALUES
('system'), -- System level account, max privileges
('trusted'), -- A trusted user, can go "negative"
('other'); -- A not fully trusted user, shouldn't go "negative"

-- List of money accounts.
-- A positive value means that there is money to take from that
-- account.
CREATE TABLE money (
    id INTEGER PRIMARY KEY NOT NULL,
    -- Name is nullable to allow "removal" of an account
    name TEXT UNIQUE,
    type INTEGER NOT NULL,

    FOREIGN KEY (type) REFERENCES money_account_type(id)
);

-- Log of money diff submissions
CREATE TABLE money_diffs (
    id INTEGER PRIMARY KEY NOT NULL,
    expected INTEGER NOT NULL,
    actual INTEGER NOT NULL,
    account INTEGER NOT NULL DEFAULT 0,
    time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,

    FOREIGN KEY (account) REFERENCES money(id)
);

-- Log of "other" money transfers
CREATE TABLE money_transfers (
    id INTEGER PRIMARY KEY NOT NULL,
    change INTEGER NOT NULL,
    -- the two accounts can be set to 0 for the "outside world"
    from_acc INTEGER NOT NULL REFERENCES money(id),
    to_acc INTEGER NOT NULL REFERENCES money(id),
    time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    note TEXT
);

-- för inköp från leverantör
-- Alla objekt vid ett inköpstillfälle kommer att ha samma 'time'
--     Och formar då ett "kvitto"
CREATE TABLE acquisitions (
    id INTEGER PRIMARY KEY NOT NULL,
    time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    product_id INTEGER NOT NULL,
    item_price INTEGER DEFAULT 0, -- price of each unit
    amount INTEGER DEFAULT 0,
    account INTEGER NOT NULL DEFAULT 0 REFERENCES money(id),

    FOREIGN KEY (product_id) REFERENCES products(id)
);

CREATE TABLE stock_diff (
    id INTEGER PRIMARY KEY NOT NULL,
    product_id INTEGER NOT NULL,
    expected INTEGER NOT NULL DEFAULT 0,
    actual INTEGER NOT NULL DEFAULT 0,
    time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,

    FOREIGN KEY (product_id) REFERENCES products(id)
);

CREATE TABLE drainage (
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    start_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    end_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    decrease INTEGER DEFAULT 2 NOT NULL,
    how_often_minutes INTEGER DEFAULT 60 NOT NULL,
    min_price INTEGER DEFAULT 5 NOT NULL
);

CREATE TABLE my_db_settings (
    id INTEGER PRIMARY KEY NOT NULL,
    name TEXT UNIQUE NOT NULL,
    value
);

-- ============================== Views ==================================

CREATE VIEW drainage_change AS -- how prices have changed due to the drainage, or null
SELECT min_price,
        ( (strftime('%s', 'now') - strftime('%s', start_time))
            / (60 * how_often_minutes)
        ) * decrease AS change
FROM drainage
WHERE CURRENT_TIMESTAMP BETWEEN start_time AND end_time;

-- The same layout as products, but with drainage prices when those are applicable
CREATE VIEW current_products AS
SELECT p.id,
       p.sorting_id,
       p.buy_id,
       p.bar_code,
       p.name,
       ifnull(MIN(p.price, MAX(d.min_price, p.price - d.change)), p.price) AS price,
       p.sale_status
FROM products p LEFT JOIN drainage_change d;

CREATE VIEW transaction_log AS
SELECT log.id
      , products.name
      , log.price / 100 AS price
      , a.name AS account
      , datetime(log.time, 'localtime') AS sold_time
FROM log
INNER JOIN products
ON log.product_id = products.id
LEFT JOIN money a
ON a.id = log.account
WHERE datetime(log.time, 'localtime')
BETWEEN datetime('now', 'localtime', '-12 hours', 'start of day', '+12 hours')
    AND datetime('now', 'localtime', '-12 hours', 'start of day', '+36 hours')
ORDER BY log.time;

CREATE VIEW money_simple AS
SELECT m.name AS name,
       printf('%.2f', a.total / 100.0) AS amount
FROM money m
LEFT JOIN account_balances s ON m.id = s.account_id
WHERE id != 0;

CREATE VIEW stock_balance AS
SELECT SUM(amount) AS total
     , product_id
FROM full_stock_log
GROUP BY product_id;

CREATE VIEW full_stock_log AS
    SELECT time AS time
         , product_id AS product_id
         , amount AS amount
    FROM acquisitions
UNION ALL
    SELECT time
         , product_id
         , -1
    FROM log
UNION ALL
    SELECT time
         , product_id
         , actual - expected
    FROM stock_diff
    ORDER BY time ASC
;


CREATE TRIGGER initialize_money_account
AFTER INSERT ON money
FOR EACH ROW
    BEGIN
        INSERT INTO money_transfers (change, from_acc, to_acc, note)
        VALUES (0, NEW.id, NEW.id, 'Initialize account');
    END;

CREATE VIEW full_money_log AS
SELECT time AS time, -(item_price*amount) AS change, account AS account
FROM acquisitions
UNION ALL
SELECT time, price, account FROM log
UNION ALL
SELECT time, - change, from_acc FROM money_transfers
UNION ALL
SELECT time, change, to_acc FROM money_transfers
UNION ALL
SELECT time, actual - expected, account FROM money_diffs
ORDER BY time ASC;

CREATE VIEW account_balances AS
SELECT account AS account_id
     , SUM(change) AS total
FROM full_money_log
GROUP BY account;

-- ============================== Triggers ===============================

CREATE TRIGGER drainage_constraint
BEFORE INSERT ON drainage
FOR EACH ROW
BEGIN
    SELECT RAISE(ABORT, 'Drainages can''t overlap')
    WHERE (SELECT SUM(NEW.start_time BETWEEN start_time AND end_time
                   OR NEW.end_time   BETWEEN start_time AND end_time)
               FROM drainage);

    SELECT RAISE(ABORT, 'New drainages can''t end in the past')
    WHERE (SELECT NEW.end_time < CURRENT_TIMESTAMP);

    SELECT RAISE(ABORT, 'Drainages need to have a positive time')
    WHERE (SELECT NEW.start_time > NEW.end_time);
END;

-- ============================== Default Queries =======================

INSERT INTO my_db_settings (name, value)
VALUES -- Account which money is drained from by default.
       -- Should be possible for the user to set (from the GUI).
       ('default_money_account', 1);

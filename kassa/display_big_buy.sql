CREATE TEMPORARY VIEW display_big_buy AS
SELECT product_id
     , buy_id
     , count(product_id) AS amount
     , name
     , price / 100 AS price_kr
     , (count(product_id) * price) / 100 AS total_kr
FROM temp.full_big_buy
GROUP BY product_id

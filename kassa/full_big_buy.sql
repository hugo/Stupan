CREATE TEMPORARY VIEW full_big_buy AS
SELECT p.id AS product_id
     , b.id AS buy_id
     , p.name AS name
     , p.price AS price
FROM temp.big_buy b
LEFT JOIN current_products p
ON b.product_id = p.id

#include "widget.h"
#include "ui_widget.h"

#include <QtCore>

// TODO more and better keybinds

// TODO select newly added item in big_buy, if possible

Widget::Widget(Configuration conf, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    ui->purchaseTab->double_click_enabled = conf.double_click_enabled;
    this->drawer = new CashDrawer(conf.port);
    QObject::connect(this, &Widget::open_till,
                     this->drawer, &CashDrawer::open_till);

    QObject::connect(ui->accountTab, &Accounts::accounts_changed,
                     ui->purchaseTab, &PurchaseTab::update_accounts);
    QObject::connect(ui->accountTab, &Accounts::account_changed,
                     ui->purchaseTab, &PurchaseTab::update_account);
    QObject::connect(ui->purchaseTab, &PurchaseTab::accounts_changed,
                     ui->accountTab, &Accounts::update_accounts);
    QObject::connect(ui->purchaseTab, &PurchaseTab::account_changed,
                     ui->accountTab, &Accounts::update_account);

}

Widget::~Widget()
{
    delete this->drawer;
    delete ui;
}


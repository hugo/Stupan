-- this was needed to have price in crown without delegate in product list
-- also, possibly rename this to product_listings
CREATE TEMPORARY VIEW product_list AS
SELECT p.id
     , p.bar_code
     , p.sorting_id
     , p.name
     , p.price / 100 AS price
     , s.total
FROM current_products p
LEFT JOIN stock_balance s ON p.id = s.product_id
WHERE p.sale_status = 0

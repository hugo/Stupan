CREATE VIEW IF NOT EXISTS total_log AS
SELECT count(products.name) AS amount, products.name
FROM log INNER JOIN products
ON log.product_id = products.id
WHERE datetime(log.time, 'localtime') BETWEEN
    datetime('now', 'localtime', '-12 hours', 'start of day', '+12 hours')
AND datetime('now', 'localtime', '-12 hours', 'start of day', '+36 hours')
GROUP BY products.id
ORDER BY products.name;

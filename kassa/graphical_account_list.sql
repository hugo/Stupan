CREATE TEMPORARY VIEW graphical_account_list AS
SELECT m.id AS money_id
     , t.id as type_id
     , m.name AS name
     , l.total AS total
     , t.name AS type
FROM money m
LEFT JOIN account_balances l ON l.account_id = m.id
LEFT JOIN money_account_type t ON m.type = t.id
ORDER BY m.name DESC

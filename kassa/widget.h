#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

#include "configuration.h"
#include "cashdrawer.h"

namespace Ui {
class Widget;
}

/**
 * @brief Main window for the Purchase system
 */
class Widget : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief Sets up window, and connect child signals.
     * @param conf Configuration to use
     * @param parent For Qt Cleanup
     */
    explicit Widget(Configuration conf, QWidget *parent = 0);
    /**
     * @brief cleanup
     */
    ~Widget();

signals:
    /**
     * @brief Request that the cash drawer gets opened.
     */
    void open_till();

private:
    Ui::Widget *ui;

    CashDrawer* drawer;
};

#endif // WIDGET_H

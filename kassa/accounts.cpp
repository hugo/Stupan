#include "accounts.h"
#include "ui_accounts.h"

#include <QSqlRelationalTableModel>
#include <QItemSelectionModel>
#include <QDebug>
#include <QSqlError>
#include <QSqlQuery>
#include <QMessageBox>

#include "moneydelegate.h"
#include "macros.h"
#include "utils.h"

#include "db_enum.h"

Accounts::Accounts(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Accounts)
{
    ui->setupUi(this);

    // util::runSqlResource(":/accounts/tables/graphical_account_list.sql");
    util::runSqlResource(":/accounts/view/graphical_account_list.sql");
    auto* accountModel = new QSqlTableModel(ui->accountList);
    accountModel->setTable("graphical_account_list");
    SET_HEADER_DATA(accountModel,
        { DB::GraphicalAccountList::Name,  tr("Namn") },
        { DB::GraphicalAccountList::Total, tr("Skuld") },
        { DB::GraphicalAccountList::Type,  tr("Kontotyp") }
       );
    //accountModel->setRelation(3, QSqlRelation("money_account_type", "id", "name"));
    ui->accountList->setSelectionBehavior(QAbstractItemView::SelectRows);
    // accountModel->setFilter("type != (SELECT id FROM money_account_type WHERE name = 'system')");
    // accountModel->setFilter("type != 'system");

    if (!accountModel->select())
        qDebug() << __FILE__ << __LINE__ << accountModel->lastError();

    ui->accountList->setModel(accountModel);
    ui->accountList->hideColumn(DB::GraphicalAccountList::MoneyId);
    ui->accountList->hideColumn(DB::GraphicalAccountList::TypeId);
    ui->accountList->setEditTriggers(QAbstractItemView::NoEditTriggers);
    /* TODO edit delegate
     * https://doc.qt.io/qt-5/qsqlrelationaltablemodel.html#details
     */
    SET_COL_DELEGATORS(ui->accountList,
        { DB::GraphicalAccountList::Total, new MoneyDelegate(this) });


    /* Remove placeholder text used in the UI builder */
    ui->accountNameLabel->setText("");
    QObject::connect(ui->accountList->selectionModel(),
                     &QItemSelectionModel::currentChanged,
                     this, &Accounts::updateCurrentAccountLabel);

    QObject::connect(this, &Accounts::account_changed,
                     this, &Accounts::update_account);
    QObject::connect(this, &Accounts::accounts_changed,
                     this, &Accounts::update_accounts);
}

Accounts::~Accounts()
{
    delete ui;
}

void Accounts::createAccount()
{
    QString name = ui->createAccountName->text();
    if (name.isEmpty()) return;
    this->createAccount(name);
}

void Accounts::createAccount(const QString name)
{
    QSqlQuery query;
    query.prepare("INSERT INTO money (name, type) "
                  "VALUES (:name, (SELECT id from money_account_type WHERE name = 'other'))");
    query.bindValue(":name", name);
    if (!query.exec()) {
        qDebug() << __FILE__ << __LINE__ << query.lastError();
        QMessageBox::warning(this, tr("Misslyckades med att lägga till konto"),
                             tr("Kunde inte skapa kontot %1.\nSQLError: %2")
                             .arg(name, query.lastError().text()));
        return;
    }

    emit accounts_changed();

    for (int i = 0; i < ui->accountList->model()->rowCount(); i++) {
        QModelIndex idx = ui->accountList
                ->model()
                ->index(i, DB::GraphicalAccountList::Name);
        QString rowName = idx.data().toString();
        if (rowName == name) {
            ui->accountList->selectionModel()->select(idx, QItemSelectionModel::Select|QItemSelectionModel::Rows);
            ui->accountList->scrollTo(idx);
        }
    }

    ui->createAccountName->setText("");
}

void Accounts::updateCurrentAccountLabel(const QModelIndex &index, const QModelIndex&)
{
    QAbstractItemModel* model = ui->accountList->model();
    // auto name = model->data(model->index(index.row(), 1)).toString();
    auto name = model->index(index.row(), DB::GraphicalAccountList::Name)
            .data().toString();
    ui->accountNameLabel->setText(name);
}

void Accounts::on_addMoneyButton_clicked()
{
    this->addMoneyToAccount();
}

void Accounts::addMoneyToAccount()
{
    long long amount = ui->addMoneyAmount->value();

    auto selected = ui->accountList->selectionModel()->selectedRows();
    if (selected.empty()) return;
    /* NOTE only first selected works. But only one can be selected */
    QModelIndex idx = ui->accountList->model()
            ->index(selected.first().row(),
                    DB::GraphicalAccountList::MoneyId);
    if (! idx.isValid()) return;

    sqlite_key acc_id = idx.data().toLongLong();

    this->addMoneyToAccount(acc_id, amount);
}

void Accounts::addMoneyToAccount(sqlite_key acc_id, long long amount)
{
    if (amount < 0) {
        QMessageBox::warning(this, tr("Negativa pengar finns inte"),
                             tr("Negative pengar finns inte, försök igen"));
        return;
    }

    QSqlQuery query;
    query.prepare("INSERT INTO money_transfers (change, from_acc, to_acc, note) "
                  "VALUES (:change, 0, :acc, 'Money added by user')");
    query.bindValue(":change", - amount * 100);
    query.bindValue(":acc", acc_id);

    if (! query.exec()) {
        qDebug() << __FILE__ << __LINE__ << query.lastError();
        QMessageBox::warning(this, tr("Misslyckades att lägga till pengar"),
                             tr("Kunde inte lägga till pengar till kontot.\nSQLError: %1")
                             .arg(query.lastError().text()));
        return;
    }

    ui->addMoneyAmount->setValue(0);

    emit account_changed(acc_id);
}


void Accounts::on_createAccountButton_clicked()
{
    this->createAccount();
}


void Accounts::on_createAccountName_returnPressed()
{
    this->createAccount();
}

void Accounts::update_account(sqlite_key)
{
    this->update_accounts();
}

void Accounts::update_accounts()
{
    static_cast<QSqlTableModel*>(ui->accountList->model())->select();
}

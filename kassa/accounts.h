#ifndef ACCOUNTS_H
#define ACCOUNTS_H

#include <QWidget>
#include "utils.h"

namespace Ui {
class Accounts;
}

/**
 * @brief Controller for the Account list.
 */
class Accounts : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief Initialize widget with the given parent for cleanup.
     * @param parent
     */
    explicit Accounts(QWidget *parent = nullptr);

    /**
     * @brief Cleanup
     */
    ~Accounts();

public slots:

    /**
     * @brief Creates a new account
     *
     * Fetches the account name from the relevant UI text box.
     */
    void createAccount();

    /**
     * @brief Creates a new account
     * @param name The name of the new account.
     */
    void createAccount(const QString name);

    /**
     * @brief Adds money to a user account.
     *
     * Fetches the values from the UI.
     */
    void addMoneyToAccount();
    /**
     * @brief Adds money to a user account.
     *
     * @param account Key into the user money table.
     * @param amount The amount to add, must be positive.
     *   A negative value will fail with a message box warning.
     *   Using an unsigned value would make it impossible to do this check.
     */
    void addMoneyToAccount(sqlite_key account, long long amount);

    /**
     * @brief Update the complete list of money accounts.
     */
    void update_accounts();

    /**
     * @brief Update this specific money account.
     * @todo actually implement this properly.
     */
    void update_account(sqlite_key);

signals:
    /**
     * @brief One or more user money accounts have changed.
     */
    void accounts_changed();

    /**
     * @brief This specific user money account has changed.
     */
    void account_changed(sqlite_key);


private:
    Ui::Accounts *ui;

private slots:
    void updateCurrentAccountLabel(const QModelIndex &current, const QModelIndex &previous);

    void on_addMoneyButton_clicked();
    void on_createAccountButton_clicked();
    void on_createAccountName_returnPressed();
};

#endif // ACCOUNTS_H

#include "widget.h"
#include <QApplication>
#include <QTranslator>
#include <QCoreApplication>
#include <QVector>

#include "configuration.h"
#include "utils.h"

#include "configuration.h"
#include "utils.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    util::check_exclusivity();

    // QLocale::setDefault(QLocale(QLocale::Swedish, QLocale::Sweden));

    QTranslator translator;
    QDate today = { QDate::currentDate() };
    auto daysTo = today.daysTo(QDate(today.year(), 4, 1));
    if (6 > daysTo && daysTo >= 0) {
        //translator.load(QStringLiteral("stupan_") + "dk", "/usr/share/stupan");
        qDebug() << QCoreApplication::applicationDirPath();
        QStringList paths;
        paths
                << QCoreApplication::applicationDirPath()
                << QStandardPaths::locateAll(QStandardPaths::DataLocation, "stupan", QStandardPaths::LocateDirectory);

        foreach (auto path, paths) {
            qDebug() << "Attempting to load translations from" << path;
            if (translator.load("stupan_dk", path)) {
                a.installTranslator(&translator);
                qDebug() << "Loaded!";
                break;
            } else {
                qDebug() << "Failed loading tranlation";
            }
        }
    }


    /*
    QString HOME = QStandardPaths::displayName(QStandardPaths::HomeLocation);
    Configuration conf(HOME + "/.stupan/config");
    */
    //Configuration conf(QStandardPaths::displayName(QStandardPaths::ConfigLocation) + "/stupan/config");

    QString HOME = QString::fromLocal8Bit(getenv("HOME"));
    Configuration conf(HOME + "/.stupan/config");

    util::setupDatabase(conf.db_name);

    Widget w(conf);
    w.show();

    return a.exec();
}

#include "purchasetab.h"
#include "ui_purchasetab.h"

#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QSqlQueryModel>
#include <QSqlTableModel>

#include <QMessageBox>
#include <QFileInfo>
#include <QDebug>
#include <QTimer>
#include <QDateTime>
#include <QMenu>

#include "utils.h"
#include "macros.h"
#include "db_enum.h"

PurchaseTab::PurchaseTab(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PurchaseTab)
{
    ui->setupUi(this);

    util::runSql("DROP TABLE IF EXISTS big_buy");
    util::runSql("DROP VIEW IF EXISTS full_big_buy");
    util::runSql("DROP VIEW IF EXISTS display_big_buy");
    util::runSql("DROP VIEW IF EXISTS display_big_buy");
    util::runSql("DROP VIEW IF EXISTS product_list");
    util::runSql("DROP TRIGGER IF EXISTS after_big_buy_delete");

    util::runSqlResource(":/purchasetab/tables/big_buy.sql");
    util::runSqlResource(":/purchasetab/view/full_big_buy.sql");
    util::runSqlResource(":/purchasetab/view/display_big_buy.sql");

    util::runSqlResource(":/purchasetab/view/total_log.sql");
    util::runSqlResource(":/purchasetab/view/product_list.sql");

    {
    QSqlTableModel* productModel = new QSqlTableModel(ui->productList);
    productModel->setTable("product_list");
    productModel->setFilter("");
    SET_HEADER_DATA(productModel,
        { DB::ProductList::SortingId, tr("#") },
        { DB::ProductList::Name,  tr("Namn") },
        { DB::ProductList::Price, tr("Pris") },
        { DB::ProductList::Total, tr("Lager") });
    if (!productModel->select())
        qDebug() << __FILE__ << __LINE__ << productModel->lastError();

    ui->productList->setModel(productModel);
    ui->productList->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->productList->header()->setSectionResizeMode(QHeaderView::ResizeToContents);
    HIDE_COLUMNS(ui->productList,
                 DB::ProductList::Id,
                 DB::ProductList::BarCode,
                 DB::ProductList::SortingId);
    }

    {
    QSqlTableModel* totalLogModel = new QSqlTableModel(ui->totalLog);
    totalLogModel->setTable("total_log");
    SET_HEADER_DATA(totalLogModel,
                    { DB::TotalLog::Amount, tr("Antal") },
                    { DB::TotalLog::Name,   tr("Namn") });
    if (!totalLogModel->select())
        qDebug() << __FILE__ << __LINE__ << totalLogModel->lastError();

    ui->totalLog->setModel(totalLogModel);
    ui->totalLog->header()->setSectionResizeMode(QHeaderView::ResizeToContents);
    }

    {
    QSqlTableModel* transactionModel = new QSqlTableModel(ui->transactionLog);
    transactionModel->setTable("transaction_log");
    SET_HEADER_DATA(transactionModel,
        { DB::TransactionLog::Name,     tr("Namn") },
        { DB::TransactionLog::Price,    tr("Pris") },
        { DB::TransactionLog::Account,  tr("Konto") },
        { DB::TransactionLog::SoldTime, tr("Klockslag") });
    if (!transactionModel->select())
        qDebug() << __FILE__ << __LINE__ << transactionModel->lastError();

    ui->transactionLog->setModel(transactionModel);
    ui->transactionLog->header()->setSectionResizeMode(QHeaderView::ResizeToContents);
    ui->transactionLog->hideColumn(DB::TransactionLog::Id);
    }

    {
    // TODO switch places for first and secound column
    QSqlTableModel* big_buy_model = new QSqlTableModel(ui->bigBuyList);
    big_buy_model->setTable("display_big_buy");
    SET_HEADER_DATA(big_buy_model,
        { DB::DisplayBigBuy::Amount,  tr("Antal") },
        { DB::DisplayBigBuy::Name,    tr("Namn") },
        { DB::DisplayBigBuy::PriceKr, tr("Pris") },
        { DB::DisplayBigBuy::TotalKr, tr("Totalt Pris") });
    if (!big_buy_model->select())
        qDebug() << __FILE__ << __LINE__ << big_buy_model->lastError();

    ui->bigBuyList->setModel(big_buy_model);
    ui->bigBuyList->header()->setSectionResizeMode(QHeaderView::ResizeToContents);
    ui->bigBuyList->header()->setSectionResizeMode(DB::DisplayBigBuy::Name,
                                                   QHeaderView::Stretch);
    ui->bigBuyList->setSelectionBehavior (QAbstractItemView::SelectRows);
    HIDE_COLUMNS(ui->bigBuyList,
                 DB::DisplayBigBuy::ProductId,
                 DB::DisplayBigBuy::BuyId);
    }

    {
    QItemSelectionModel* selectionModel = ui->productList->selectionModel();
    QObject::connect(
                selectionModel, &QItemSelectionModel::currentChanged,
                /* TODO change this to updateAccountList */
                this, &PurchaseTab::on_left_changed);
    }

    lastDrainageId = QVariant(-1);
    drainageTimer = new QTimer(this);
    QObject::connect(drainageTimer, &QTimer::timeout,
                     this, &PurchaseTab::requery_views);

    check_drainage();
    QTimer* timer = new QTimer(this);
    QObject::connect(timer, &QTimer::timeout,
                     this, &PurchaseTab::check_drainage);
    timer->start(300 * 1000);

    ui->buyStatusLabel->setText("");

    /* --- */

    this->setup_account_dropdown();

    QObject::connect(this, &PurchaseTab::account_changed,
                     this, &PurchaseTab::update_account);
    QObject::connect(this, &PurchaseTab::accounts_changed,
                     this, &PurchaseTab::update_accounts);

}

PurchaseTab::~PurchaseTab()
{
    delete ui;
}

void PurchaseTab::on_left_changed(const QModelIndex &selected, const QModelIndex& /*deselected*/)
{
    ui->sellButton->setEnabled(true);
    if (! ui->bigBuyList->isEnabled()) {
        set_change_strings(get_single_price(selected).toInt());
    }
}

/*
 * Have you heard the tragedy of Maybe Darth -> The Wise (?)
 */
QVariant PurchaseTab::get_single_price(const QModelIndex& in_id)
{
    //int price = ui->productList->model()->index(ui->productList->currentIndex().row(), 3).data().toInt();
    int row = in_id.row();
    QModelIndex idx = ui->productList->model()->index(row, DB::ProductList::Price);
    if (idx.isValid()) {
        return ui->productList->model()->data(idx);
    }
    // null int
    return QVariant(QVariant::Int);
}

void PurchaseTab::on_sellButton_clicked()
{
    /* TODO magic number */
    just_sell_it(1);
}

void PurchaseTab::on_altSellButton_clicked()
{
    /* TODO magic number */
    just_sell_it(1);
}

/*
 * This is for double click, which should be disabled
 */
void PurchaseTab::on_productList_activated(const QModelIndex& /*index*/)
{
    if (this->double_click_enabled) {
        if (ui->bigBuyList->isEnabled()
                && ui->bigBuyList->model()->rowCount() != 0) {
            add_product_bigBuy();
        } else {
            /* TODO magic number */
            just_sell_it(1);
        }
    }
}

void PurchaseTab::on_searchEdit_textEdited(const QString& arg1)
{
    update_for_sale(arg1);
}

void PurchaseTab::on_openButton_clicked()
{
    emit open_till();
}


void PurchaseTab::on_addProduct_clicked()
{
    add_product_bigBuy();
    ui->RightHalf->setCurrentIndex(1);
}

void PurchaseTab::remove_selected_from_bigBuy() {
    QModelIndex index;
    // only one row can be selected at a time, but this provides a safeguard
    foreach ( QModelIndex i, ui->bigBuyList->selectionModel()->selectedRows() ) {
        index = ui->bigBuyList->model()->index(i.row(), DB::DisplayBigBuy::BuyId);
        QVariant id = index.data();

        QSqlQuery query;
        query.prepare("DELETE FROM big_buy WHERE id = :id");
        query.bindValue(":id", id);
        if(!query.exec())
            qDebug() << __FILE__ << __LINE__ << query.lastError();
    }

    requery_bigBuy(true);

    // disable if no products left, return to singles mode
    if (ui->bigBuyList->model()->rowCount() == 0) {
        ui->bigBuyList->setEnabled(false);
        ui->altSellButton->setEnabled(false);
    }

    ui->RightHalf->setCurrentIndex(1);
    update_sumChange_strings();
}


void PurchaseTab::on_removeProduct_clicked()
{
    if (! ui->bigBuyList->isEnabled()) return;

    remove_selected_from_bigBuy();
}

void PurchaseTab::cancel_selected_purchases()
{
    foreach (QModelIndex item, ui->transactionLog->selectionModel()->selectedRows(0)) {
        // removes from the database
        QVariant id = item.data(DB::TransactionLog::Id);

        // money var is updated through trigger
        QSqlQuery deleteQuery;
        deleteQuery.prepare("DELETE FROM log WHERE id = :id");
        deleteQuery.bindValue(":id", id);
        if (!deleteQuery.exec())
            qDebug() << __FILE__ << __LINE__ << deleteQuery.lastError();

        // removes it from the ui, now done in the update loop
        //delete &item;
    }

    emit accounts_changed();
}

/*
 * Removes from the log,
 * select what to remove from the transactionLog, and confirm with the button
 */
void PurchaseTab::on_cancelBuyButton_clicked()
{
    cancel_selected_purchases();
    emit open_till();
    requery_transactionLog();
    requery_totalLog();
}

void PurchaseTab::cancel_last_big_buy() {
    if (amount_of_products_last_buy <= 0) return;

    QSqlQuery delete_query;
    delete_query.prepare("DELETE FROM log WHERE id IN "
                         "(SELECT id FROM log"
                         " ORDER BY id DESC"
                         " LIMIT :limit)");

    delete_query.bindValue(":limit", amount_of_products_last_buy);
    if (!delete_query.exec())
        qDebug() << __FILE__ << __LINE__ << delete_query.lastError();

    ui->buyStatusLabel->setText("");

    emit accounts_changed();
}

void PurchaseTab::on_cancelBigBuyButton_clicked()
{
    cancel_last_big_buy();

    ui->cancelBigBuyButton->setEnabled(false);

    requery_bigBuy(true);

    requery_transactionLog();
    requery_totalLog();
}

// === requery ===

void PurchaseTab::check_drainage()
{
    // queries new products, this is also done whenever
    // a product is sold, or the search is used.
    requery_productList();

    QSqlQuery query;
    query.exec("SELECT id,"
               "       strftime('%s', start_time),"
               "       strftime('%s', end_time),"
               "       how_often_minutes "
               "FROM drainage WHERE CURRENT_TIMESTAMP "
               "BETWEEN start_time AND end_time");

    if (query.next()) {

        QVariant drainageId = query.value(0);

        if (drainageId == lastDrainageId)
            return;

        lastDrainageId  = drainageId;
        long long start = query.value(1).toLongLong();
        // long long end   = query.value(2).toLongLong();
        int how_often   = query.value(3).toInt() * 60;

        drainageTimer->stop();
        drainageTimer->setInterval(how_often * 1000);

        long long now = QDateTime::currentMSecsSinceEpoch() / 1000ll;
        int time_until_first_sleep = how_often - ((now - start) % how_often);

        QTimer* timeout = new QTimer(this);
        timeout->setSingleShot(true);
        connect(timeout, &QTimer::timeout, this, &PurchaseTab::drainage_helper);
        //connect(timeout, &QTimer::timeout, this, &[](this){ drainageTimer->start(); });
        timeout->start((time_until_first_sleep + 2) * 1000);
    }
}

// this is spammed
void PurchaseTab::drainage_helper()
{
    drainageTimer->start();
}

void PurchaseTab::requery_views()
{
    requery_bigBuy(false);
    requery_productList();
}

/*
 * This usually only updates if bigBuy is enabled
 * The force flag overrides this.
 */
void PurchaseTab::requery_bigBuy(bool force)
{
    bool state = ui->bigBuyList->isEnabled();
    ui->bigBuyList->setEnabled(force ? true : state);
    if (ui->bigBuyList->isEnabled())
        util::requery_sql_model(ui->bigBuyList->selectionModel(),
                                static_cast<QSqlTableModel*>(ui->bigBuyList->model()));
    ui->bigBuyList->setEnabled(state);
}

void PurchaseTab::requery_productList()
{
    util::requery_sql_model(ui->productList->selectionModel(),
                            static_cast<QSqlTableModel*>(ui->productList->model()));
}

void PurchaseTab::requery_transactionLog()
{
    util::requery_sql_model(ui->transactionLog->selectionModel(),
                            static_cast<QSqlTableModel*>(ui->transactionLog->model()));
    // TODO scroll to bottom if at bottom beforehand
    // TODO or possibly just let new items be at the top
    //ui->transactionLog->scrollToBottom();
}

void PurchaseTab::requery_totalLog()
{
    util::requery_sql_model(ui->totalLog->selectionModel(),
                            static_cast<QSqlTableModel*>(ui->totalLog->model()));
}

/*
void PurchaseTab::requery_changeStrings()
{
    if (! ui->bigBuyList->isEnabled()) {
        //int price = ui->productList->model()->index(ui->productList->currentIndex().row(), 3).data().toInt();
        int row = ui->productList->currentIndex().row();
        QModelIndex idx = ui->productList->model()->index(row, 4);
        if (idx.isValid()) {
                int price = ui->productList->model()->data(idx).toInt();
                set_change_strings(price);
        }
    } else {
        update_sumChange_strings();
    }
}
*/


// === Other ===

/*
 * Sets the strings in the lower left to show propper change
 * also toggles the line between the two change values
 *
 * Sets the bigBuySum
 *
 * Note that it takes the price in crowns
 */
void PurchaseTab::set_change_strings(int price) {
    int up100 = util::round_up_n(price, 100);
    auto str1 = QString (tr("%1 - %2 = %3"))
            .arg(up100)
            .arg(price)
            .arg(up100 - price);
    ui->changeLabel100->setText(str1);

    int up50 = util::round_up_n(price, 50);
    if (up50 != up100 && up50 - price != 0) {
        ui->changeSeparator50->setHidden(false);
        auto str2 = QString(tr("%1 - %2 = %3"))
                .arg(up50)
                .arg(price)
                .arg(up50 - price);
        ui->changeLabel50->setText(str2);
    } else {
        ui->changeSeparator50->setHidden(true);
        ui->changeLabel50->setText("");
    }

    int up20 = util::round_up_n(price, 20);
    if (up20 != up100 && up20 - price != 0) {
        ui->changeSeparator20->setHidden(false);
        auto str2 = QString(tr("%1 - %2 = %3"))
                .arg(up20)
                .arg(price)
                .arg(up20 - price);
        ui->changeLabel20->setText(str2);
    } else {
        ui->changeSeparator20->setHidden(true);
        ui->changeLabel20->setText("");
    }

    ui->bigBuySum->setText(QString("%1").arg(price));
}


// sell currently selected item
void PurchaseTab::just_sell_it(sqlite_key account_id) {

    QList<sqlite_key> items;

    // adds single item to big buy before imidiately purchasing
    if (ui->bigBuyList->model()->rowCount() == 0 || ! ui->bigBuyList->isEnabled()) {
        QAbstractItemModel* model = ui->productList->model();
        sqlite_key id = model->data(model->index(ui->productList->currentIndex().row(),
                                                 DB::ProductList::Id)).toLongLong();
        if (id == 0) return;
        add_product_bigBuy(id);
    }

    /* This should show that the system is working, but it doesn't update here...
     * It however kind of works out, since we clear it earlier, and update it to
     * show that the transaction is done once it is.
     */
    ui->buyStatusLabel->setText(tr("Behandlar..."));

    QSqlQuery query("SELECT product_id FROM big_buy");
    while(query.next()) {
        items.append(query.value(0).toLongLong());
    }

    // this is for cancelling sells
    amount_of_products_last_buy = items.length();

    //ui->cancelBigBuyButton->setEnabled(true);
    requery_bigBuy(true);
    ui->cancelBigBuyButton->setEnabled(true);
    ui->altSellButton->setEnabled(false);
    ui->sellButton->setEnabled(false);

    ui->bigBuyList->setEnabled(true);

    QSqlDatabase::database().transaction();

    QSqlQuery query2;
    query2.prepare("INSERT INTO log (product_id, price, account) "
                   "SELECT product_id, price, :account "
                   "FROM full_big_buy");
    query2.bindValue(":account", account_id);
    query2.exec();

    QSqlQuery("DELETE FROM big_buy");

    if (! QSqlDatabase::database().commit()) {
        qDebug() << __FILE__
                 << __LINE__
                 << "Big buy transaction failed"
                 << QSqlDatabase::database().lastError();
    }
    ui->bigBuyList->setEnabled(false);

    ui->searchEdit->clear();

    if (items.length() == 0) return;

    update_for_sale();

    ui->RightHalf->setCurrentIndex(1);

    emit open_till();

    requery_transactionLog();
    requery_totalLog();

    ui->buyStatusLabel->setText(tr("Köpt!"));

    emit account_changed(account_id);
}

void PurchaseTab::update_for_sale()
{
    PurchaseTab::update_for_sale("");
}

// this requries the left one
void PurchaseTab::update_for_sale(const QString& search) {
    // TODO TODO possible injection hole here!!!!
    // Typing '; (apostrophe, semicolon) seems to brake it
    // TODO possibly interspace search with '%' to get a fuzzy finder
    QString str = QString("name LIKE '%' || '%1' || '%' OR bar_code = '%1'")
                .arg(search);
    //qDebug() << __LINE__ << str;

    static_cast<QSqlTableModel*>(ui->productList->model())->setFilter(str);
}

void PurchaseTab::add_product_bigBuy()
{
    ui->buyStatusLabel->setText("");
    sqlite_key id = ui->productList->model()->index(
                ui->productList->currentIndex().row(),
                DB::ProductList::Id)
            .data().toLongLong();

    if (id == 0) return;

    ui->bigBuyList->setEnabled(true);
    ui->altSellButton->setEnabled(true);
    ui->sellButton->setEnabled(true);
    ui->cancelBigBuyButton->setEnabled(false);

    add_product_bigBuy(id);
}

void PurchaseTab::add_product_bigBuy(sqlite_key product_id)
{
    QSqlQuery query;
    query.prepare("INSERT INTO big_buy "
                  "(product_id) VALUES (:id)");
    query.bindValue(":id", product_id);
    if (!query.exec())
        qDebug() << __FILE__ << __LINE__ << query.lastError();

    requery_bigBuy(true);
    update_sumChange_strings();
}


/*
 * If bigBuy contains something sets ALL the change strings to its sum
 */
void PurchaseTab::update_sumChange_strings()
{
    int price;
    // if bigBuy contains anything
    if (ui->bigBuyList->selectionModel()->model()->rowCount() != 0) {
        QSqlQuery query("SELECT sum(price) FROM full_big_buy");

        query.next();
        price = query.value(0).toInt() / 100;
    } else foreach (QModelIndex idx, ui->productList->selectionModel()->selectedRows()) {
        /* There should always be exactly one row selected in the the
         * product list in this when this code is reached */
        price = get_single_price(idx).toInt();
    }
    set_change_strings(price);
}

void PurchaseTab::sell_through_account(sqlite_key id)
{
    just_sell_it(id);
}

void PurchaseTab::update_account(sqlite_key)
{
    this->update_accounts();
}

void PurchaseTab::update_accounts()
{
    this->setup_account_dropdown();
}

void PurchaseTab::setup_account_dropdown() {
    QSqlQuery users("SELECT m.id, m.name, b.total "
                    "FROM money m "
                    "LEFT JOIN account_balances b "
                    "ON m.id = b.account_id "
                    "WHERE m.type != 1 " // TODO magic number
                    "ORDER BY m.name ASC");

    QMenu* creditMenu = new QMenu(this);

    while (users.next()) {
        /* TODO possibly color the amount red or green depending on status */
        auto msg = tr("%1 %2kr").arg(users.value(1).toString())
                                .arg(users.value(2).toLongLong() / 100.0, 2);
        QAction* action = new QAction(msg, creditMenu);
        int user_id = users.value(0).toInt();
        QObject::connect(action, &QAction::triggered,
                         this,
                         [this, user_id](bool) {
                             this->sell_through_account(user_id);
                         });
        creditMenu->addAction(action);
    }

#if 0
    if (ui->creditButton->menu()) {
        delete ui->creditButton->menu();
    }

    if (ui->creditButton2->menu()) {
        ui->creditButton2->menu();
    }
#endif

    ui->creditButton->setMenu(creditMenu);
    ui->creditButton2->setMenu(creditMenu);
}

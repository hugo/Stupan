-- Table containing products a user wants to purchase at the same time.
CREATE TEMPORARY TABLE big_buy
        ( id INTEGER PRIMARY KEY NOT NULL
        , product_id INTEGER NOT NULL
        )
-- Foregin key references appear to be impossible between databases,
-- but please prove me wrong. */
-- , FOREIGN KEY (product_id) REFERENCES main.products(id)

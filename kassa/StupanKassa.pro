#-------------------------------------------------
#
# Project created by QtCreator 2017-06-28T17:42:43
#
#-------------------------------------------------

QT       += core gui sql serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = stupan-kassa
TEMPLATE = app

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS


setup_enum.output = db_enum.h
setup_enum.commands = $$PWD/../setup_enum.py ${QMAKE_FILE_NAME}
setup_enum.dependency_type = TYPE_C
setup_enum.name = "Generating SQLite table enums."
setup_enum.input = RESOURCES
setup_enum.depend_command = ls -1 $$PWD/*.sql
setup_enum.CONFIG += combine dep_lines
# Don't add db_enum.h to OBJECTS
setup_enum.variable_out =
QMAKE_EXTRA_COMPILERS += setup_enum

target.path = /usr/bin

INSTALLS += target

QMAKE_CXXFLAGS += -Wall -pedantic

SOURCES += main.cpp \
		   accounts.cpp \
		   purchasetab.cpp \
		   widget.cpp

HEADERS += widget.h \
    accounts.h \
    purchasetab.h

FORMS   += widget.ui \
    accounts.ui \
    purchasetab.ui

TRANSLATIONS += stupan_dk.ts

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../StupanCommon/release/ -lStupanCommon
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../StupanCommon/debug/ -lStupanCommon
else:unix: LIBS += -L$$OUT_PWD/../StupanCommon/ -lStupanCommon

INCLUDEPATH += $$PWD/../StupanCommon
DEPENDPATH += $$PWD/../StupanCommon

RESOURCES += \
    sql.qrc

DISTFILES += \
    big_buy.sql \
    display_big_buy.sql \
    full_big_buy.sql \
    graphical_account_list.sql \
    product_list.sql \
    total_log.sql

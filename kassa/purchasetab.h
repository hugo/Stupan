#ifndef PURCHASETAB_H
#define PURCHASETAB_H

#include <QWidget>
#include <QTreeWidgetItem>
#include <QSqlQueryModel>

#include "utils.h"

namespace Ui {
class PurchaseTab;
}

/**
 * @brief Controller for the primary purchase view.
 *
 * Includes both the purchase menus, as well as the history menus.
 */
class PurchaseTab : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief Initialize the widget
     * Also does some database cleanup, and creates a number of temporary
     * database objects. These aren't cleaned up by us, but rather automatically
     * when we disconnect from SQLite.
     * @param parent For Qt cleanup.
     */
    explicit PurchaseTab(QWidget *parent = nullptr);
    /**
     * @brief Cleanup
     */
    ~PurchaseTab();

    /**
     * @brief Should double click activate menus.
     *
     * See the configuration for further details.
     */
    bool double_click_enabled;

signals:
    /**
     * @brief Request that the cash till gets opened.
     */
    void open_till();

    /**
     * @brief One or more money accounts have changed.
     */
    void accounts_changed();

    /**
     * @brief This specific money account have changed.
     * @param user Primary key for the money account.
     */
    void account_changed(sqlite_key user);

public slots:
    /**
     * @brief Sell the currently selected product(s).
     *
     * If the big buy menu is empty, then the selected product from the product list
     * is used. If there is at least one product in the big buy list, then the product
     * list is ignored.
     *
     * @param id Money account to take money from.
     */
    void sell_through_account(sqlite_key id);

    /**
     * @brief Update the list of money accounts.
     */
    void update_accounts();

    /**
     * @brief Update one specific money account.
     * @param acc The account to update.
     */
    void update_account(sqlite_key acc);

private slots:
    void on_sellButton_clicked();
    void on_productList_activated(const QModelIndex &index);
    void on_searchEdit_textEdited(const QString &arg1);
    void on_openButton_clicked();
    void on_addProduct_clicked();
    void on_removeProduct_clicked();
    void on_cancelBuyButton_clicked();
    void on_cancelBigBuyButton_clicked();
    void on_altSellButton_clicked();
    void on_left_changed(const QModelIndex& selected, const QModelIndex& deselected);
    void check_drainage();
    void drainage_helper();
    void requery_views();

private:
    Ui::PurchaseTab *ui;

    int amount_of_products_last_buy = 0;

    QVariant lastDrainageId;
    QTimer* drainageTimer;

    void requery_bigBuy(bool);
    void requery_transactionLog();
    void requery_totalLog();
    void requery_productList();
    //void requery_changeStrings();

	QVariant get_single_price(const QModelIndex& idx);

    void update_for_sale();
	void update_for_sale(const QString &QString);

    void update_sumChange_strings();
    void set_change_strings(int price);

    void add_product_bigBuy();
    void add_product_bigBuy(sqlite_key product_id);

    void just_sell_it(sqlite_key account_id);

	void remove_selected_from_bigBuy();
	void cancel_selected_purchases();
	void cancel_last_big_buy();

    void setup_account_dropdown();
};

#endif // PURCHASETAB_H

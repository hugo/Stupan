<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="da" sourcelanguage="sv">
<context>
    <name>Accounts</name>
    <message>
        <location filename="accounts.ui" line="11"/>
        <source>Det här lägger till pengar för en person. Fyll i hur många kronor personer ger dig (kontant).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="accounts.ui" line="29"/>
        <source>Aktivt konto:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="accounts.ui" line="42"/>
        <source>[Namn Här]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="accounts.ui" line="53"/>
        <source>kr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="accounts.ui" line="63"/>
        <source>Lägg till Pengar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="accounts.ui" line="87"/>
        <source>Skapa</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="accounts.ui" line="94"/>
        <source>Nytt Kontonamn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="accounts.cpp" line="28"/>
        <source>Namn</source>
        <translation type="unfinished">ᚾᚨᛗᚾ</translation>
    </message>
    <message>
        <location filename="accounts.cpp" line="29"/>
        <source>Skuld</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="accounts.cpp" line="30"/>
        <source>Kontotyp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="accounts.cpp" line="83"/>
        <source>Misslyckades med att lägga till konto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="accounts.cpp" line="84"/>
        <source>Kunde inte skapa kontot %1.
SQLError: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="accounts.cpp" line="128"/>
        <source>Negativa pengar finns inte</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="accounts.cpp" line="129"/>
        <source>Negative pengar finns inte, försök igen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="accounts.cpp" line="152"/>
        <source>Misslyckades att lägga till pengar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="accounts.cpp" line="153"/>
        <source>Kunde inte lägga till pengar till kontot.
SQLError: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PurchaseTab</name>
    <message>
        <location filename="purchasetab.ui" line="17"/>
        <source>Sök...</source>
        <translation type="unfinished">ᛊᛟᚲ ...</translation>
    </message>
    <message>
        <location filename="purchasetab.ui" line="57"/>
        <location filename="purchasetab.ui" line="373"/>
        <source>Kontant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="purchasetab.ui" line="64"/>
        <location filename="purchasetab.ui" line="380"/>
        <source>Kredit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="purchasetab.ui" line="232"/>
        <source>Logg</source>
        <translation type="unfinished">ᛚᛟᚷᚷ</translation>
    </message>
    <message>
        <location filename="purchasetab.ui" line="270"/>
        <source>Häfv Köp</source>
        <translation type="unfinished">ᚺᛖᚨᚠᚹ ᚲᛟᛈ</translation>
    </message>
    <message>
        <location filename="purchasetab.ui" line="298"/>
        <source>Öppna kassan</source>
        <translation type="unfinished">ᛟᛈᛈᚾᚨ ᚲᚨᛊᛊᚨᛚᚨᛞᚨᚾ</translation>
    </message>
    <message>
        <location filename="purchasetab.ui" line="306"/>
        <source>Beställning</source>
        <translation type="unfinished">ᛒᚨᛊᛏᚨᛚᛚᚾᛁᛜ</translation>
    </message>
    <message>
        <location filename="purchasetab.ui" line="327"/>
        <source>Totalt:</source>
        <translation type="unfinished">ᛏᛟᛏᚨᛚᛏ:</translation>
    </message>
    <message>
        <location filename="purchasetab.ui" line="390"/>
        <source>Häv Köp</source>
        <translation type="unfinished">ᚺᛖᚨᚠᚹ ᚲᛟᛈ</translation>
    </message>
    <message>
        <location filename="purchasetab.cpp" line="46"/>
        <source>#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="purchasetab.cpp" line="47"/>
        <location filename="purchasetab.cpp" line="67"/>
        <location filename="purchasetab.cpp" line="79"/>
        <location filename="purchasetab.cpp" line="97"/>
        <source>Namn</source>
        <translation type="unfinished">ᚾᚨᛗᚾ</translation>
    </message>
    <message>
        <location filename="purchasetab.cpp" line="48"/>
        <location filename="purchasetab.cpp" line="80"/>
        <location filename="purchasetab.cpp" line="98"/>
        <source>Pris</source>
        <translation type="unfinished">ᛈᚱᛁᛊ</translation>
    </message>
    <message>
        <location filename="purchasetab.cpp" line="49"/>
        <source>Lager</source>
        <translation type="unfinished">ᛚᚨᚷᛖᚱ</translation>
    </message>
    <message>
        <location filename="purchasetab.cpp" line="66"/>
        <location filename="purchasetab.cpp" line="96"/>
        <source>Antal</source>
        <translation type="unfinished">ᚨᚾᛏᚨᛚ</translation>
    </message>
    <message>
        <location filename="purchasetab.cpp" line="81"/>
        <source>Konto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="purchasetab.cpp" line="82"/>
        <source>Klockslag</source>
        <translation type="unfinished">ᛏᛁᛞᛈᚢᚾᚲᛏ</translation>
    </message>
    <message>
        <location filename="purchasetab.cpp" line="99"/>
        <source>Totalt Pris</source>
        <translation type="unfinished">ᛏᛟᛏᚨᛚᛏ ᛈᚱᛁᛊ</translation>
    </message>
    <message>
        <location filename="purchasetab.cpp" line="433"/>
        <location filename="purchasetab.cpp" line="442"/>
        <location filename="purchasetab.cpp" line="455"/>
        <source>%1 - %2 = %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="purchasetab.cpp" line="487"/>
        <source>Behandlar...</source>
        <translation type="unfinished">ᛒᛖᚺᚨᚾᛞᛚᚨᚱ...</translation>
    </message>
    <message>
        <location filename="purchasetab.cpp" line="537"/>
        <source>Köpt!</source>
        <translation type="unfinished">Köpt</translation>
    </message>
    <message>
        <location filename="purchasetab.cpp" line="638"/>
        <source>%1 %2kr</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="widget.ui" line="14"/>
        <location filename="widget.ui" line="28"/>
        <source>Kassa</source>
        <translation>ᚲᚨᛊᛊᚨ</translation>
    </message>
    <message>
        <location filename="widget.ui" line="33"/>
        <source>Konton</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widget.ui" line="38"/>
        <source>Om</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sök...</source>
        <translation type="vanished">ᛊᛟᚲ ...</translation>
    </message>
    <message>
        <source>Köp</source>
        <translation type="vanished">ᚲᛟᛈ</translation>
    </message>
    <message>
        <source>Logg</source>
        <translation type="vanished">ᛚᛟᚷᚷ</translation>
    </message>
    <message>
        <source>Häfv Köp</source>
        <translation type="vanished">ᚺᛖᚨᚠᚹ ᚲᛟᛈ</translation>
    </message>
    <message>
        <source>Öppna kassan</source>
        <translation type="vanished">ᛟᛈᛈᚾᚨ ᚲᚨᛊᛊᚨᛚᚨᛞᚨᚾ</translation>
    </message>
    <message>
        <source>Beställning</source>
        <translation type="vanished">ᛒᚨᛊᛏᚨᛚᛚᚾᛁᛜ</translation>
    </message>
    <message>
        <source>Totalt:</source>
        <translation type="vanished">ᛏᛟᛏᚨᛚᛏ:</translation>
    </message>
    <message>
        <source>Häv Köp</source>
        <translation type="vanished">ᚺᛖᚨᚠᚹ ᚲᛟᛈ</translation>
    </message>
    <message>
        <source>Namn</source>
        <translation type="vanished">ᚾᚨᛗᚾ</translation>
    </message>
    <message>
        <source>Pris</source>
        <translation type="vanished">ᛈᚱᛁᛊ</translation>
    </message>
    <message>
        <source>Lager</source>
        <translation type="vanished">ᛚᚨᚷᛖᚱ</translation>
    </message>
    <message>
        <source>Antal</source>
        <translation type="vanished">ᚨᚾᛏᚨᛚ</translation>
    </message>
    <message>
        <source>Klockslag</source>
        <translation type="vanished">ᛏᛁᛞᛈᚢᚾᚲᛏ</translation>
    </message>
    <message>
        <source>Totalt Pris</source>
        <translation type="vanished">ᛏᛟᛏᚨᛚᛏ ᛈᚱᛁᛊ</translation>
    </message>
    <message>
        <source>Behandlar...</source>
        <translation type="vanished">ᛒᛖᚺᚨᚾᛞᛚᚨᚱ...</translation>
    </message>
    <message>
        <source>Köpt!</source>
        <translation type="vanished">Köpt</translation>
    </message>
</context>
</TS>

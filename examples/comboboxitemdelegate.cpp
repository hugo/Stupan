auto cbiDel = new ComboBoxItemDelegate(
			{{ SaleStatus::for_sale,     tr("Till salu") },
			 { SaleStatus::not_for_sale, tr("Ej till salu") },
			 { SaleStatus::hidden,       tr("Dold") }}, this);

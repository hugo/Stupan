#ifndef MAYBEVALIDATORDELEGATE_H
#define MAYBEVALIDATORDELEGATE_H

#include "validatordelegate.h"

/**
 * @brief Validator delegate which also allows the empty value.
 *
 * Creates a validator delegate for a validator which acceps a value, or nothing.
 * Useful for nullable fields.
 *
 * The empty string is passed as the null value.
 */
class MaybeValidatorDelegate : public ValidatorDelegate
{
public:
    /**
     * @brief MaybeValidatorDelegate
     * @param validator
     * @param parent
     */
    MaybeValidatorDelegate(QValidator* validator, QObject* parent = nullptr);

    /**
     * @brief setModelData
     * @param editor
     * @param model
     * @param index
     */
    virtual void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
};

#endif // MAYBEVALIDATORDELEGATE_H

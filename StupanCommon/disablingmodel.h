#ifndef DISABLINGVIEW_H
#define DISABLINGVIEW_H

#include <QSqlRelationalTableModel>
#include <QSqlDatabase>

/*
 * An extension of QSqlRelationalTableModel where some columns can be
 * set to be non editing
 */
/**
 * @brief A QSqlRelationalTableModel where specific columns can be disabled.
 *
 * Generally the only the whole table can be enabled or disabled. This allows creating
 * a model where some columns are disabled for editing, while the remaining allow editing.
 *
 * The set of Qt::ItemFlags used for the active and inactive columns can be changed by
 * simply setting the fields.
 */
class DisablingModel : public QSqlRelationalTableModel
{
public:
    /**
     * @brief Constructs the model
     * @param disabledColumns Indexes which shouldn't be editable.
     * @param parent For Qt Cleanup.
     * @param db Active connection for underlying QSqlTableModel
     */
    DisablingModel(QList<int> disabledColumns, QObject* parent = nullptr, QSqlDatabase db = QSqlDatabase());

    /**
     * @brief Flags used for active columns.
     */
    Qt::ItemFlags activeFlags = Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;
    /**
     * @brief Flags used for inactive columns.
     */
    Qt::ItemFlags inactiveFlags = Qt::ItemIsSelectable | Qt::ItemIsEditable;

    /**
     * @brief Flags for the given index.
     * @param index row and column to index.
     * @return either activeFlags, or inactiveFlags
     */
    virtual Qt::ItemFlags flags (const QModelIndex &index) const;

private:
    QList<int> disabledColumns;
};
#endif // DISABLINGVIEW_H

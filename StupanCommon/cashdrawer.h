#ifndef CASHDRAWER_H
#define CASHDRAWER_H

#include <QtCore>
#include <QSerialPort>

/**
 * @brief Manages a connection to a serial port especially for a cash drawer.
 */
class CashDrawer : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief Constructs a new serial port controller.
     * Note that this doesn't open the port, but instead defers that to right before
     * opening. This to not hog the resource, since serial ports don't like being
     * opened multiple times.
     * @param portname Path to the serial port.
     * @param times How many pulses should be sent to the port.
     */
    CashDrawer(QString portname, int times = 1);

public slots:
    /**
     * @brief Open the cash drawer till.
     */
    void open_till();

private slots:
    void afterSerialWrite(qint64 bytes);

private:
    QSerialPort serialPort;
    int times;



};

#endif // CASHDRAWER_H

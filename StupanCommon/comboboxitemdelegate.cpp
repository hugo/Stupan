#include "comboboxitemdelegate.h"

#include <QComboBox>
#include <QtCore>

#include <QTableView>

ComboBoxItemDelegate::ComboBoxItemDelegate(QMap<int, QString> options, QObject* parent)
    : QStyledItemDelegate(parent)
    , options(options) {}

QWidget* ComboBoxItemDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem& /*option*/, const QModelIndex& /*index*/) const
{
    QComboBox* cb = new QComboBox(parent);
    foreach (QString value, options)
        cb->addItem(value);
    return cb;
}

// Sets data when clicked
void ComboBoxItemDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    if (QComboBox* cb = qobject_cast<QComboBox*>(editor)) {
        cb->setCurrentIndex(index.data().toInt());
    } else
        QStyledItemDelegate::setEditorData(editor, index);
}

// sets return data
void ComboBoxItemDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    if (QComboBox* cb = qobject_cast<QComboBox*>(editor))
        model->setData(index, cb->currentIndex(), Qt::EditRole);
    else
        QStyledItemDelegate::setModelData(editor, model, index);
}

// sets what's displayed when not in edit mode
QString ComboBoxItemDelegate::displayText(const QVariant &value, const QLocale& /*locale*/) const
{
    return get_index_string(value.toInt());
}

QString ComboBoxItemDelegate::get_index_string(int index) const {
    return options[index];
}

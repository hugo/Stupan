#include "aboutpage.h"
#include "ui_aboutpage.h"

#include <QDateTime>
#include <QDebug>

#include "version.h"

AboutPage::AboutPage(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AboutPage)
{
    ui->setupUi(this);

    ui->version->setText(version());

    auto dt = QDateTime::fromString(__DATE__ " " __TIME__, "MMM d yyyy hh:mm:ss");
    ui->buildDate->setText(dt.toString(tr("yyyy-MM-dd hh:mm", "När programmet senast byggdes")));

    ui->db_version->setText(QString("%1 (%2)")
                            .arg(get_db_version_raw())
                            .arg(get_db_version()));

    /* @todo
     * Futher licensing information, including:
     * - License of this product
     * - License of tableprinter
     * - (license of qt?)
     * - (license of VG artwork?)
     */
}

AboutPage::~AboutPage()
{
    delete ui;
}

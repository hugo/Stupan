#ifndef UTILS
#define UTILS

#include <QSqlQuery>
#include <QTreeWidget>
#include <QTreeWidget>
#include <QSqlQueryModel>
#include <QItemSelectionModel>
#include <QSqlTableModel>
#include <QComboBox>
#include <QFile>

/**
 * @brief Type of SQLite primary keys.
 *
 * The SQLite documentation promises that key's are always 64 bit signed integers.
 */
typedef qint64 sqlite_key;

namespace util {

/**
 * @brief Rounds number upwards to the nearest multiple of round_value.
 * @param number Number to round
 * @param round_value "Resolution" of the result.
 * @return the rounded value.
 */
int round_up_n(int number, int round_value);
void requery_sql_view(QTreeView* view, QSqlQueryModel* model);
void requery_sql_model(QItemSelectionModel* selectionModel, QSqlTableModel* model);

/**
 * @brief Runs a SQL query, logging potential errors.
 */
void runSql(const QString&);

/**
 * @brief Read SQL query from file (or resource), run it and log any errors.
 */
void runSqlResource(QFile&);

/**
 * @brief Open path, and run SQL query.
 * @param path File (or resource) to open.
 */
void runSqlResource(const QString& path);

/**
 * @brief Formats the value to 2 decimal places.
 * Doesn't add any monetary pre- or suffix.
 * @param in Monetary amount, in dollar equivalent.
 * @return the formatted value.
 */
QString formatMoney(double in);

/**
 * @brief Populate a combo box with all money accounts.
 * @param box The combo box to popuplate.
 */
void populateMoneyComboBox(QComboBox* box);

/**
 * @brief Attempt to acquire an exclusivity lock.
 *
 * Attempts to create a PID file at ${RUNTIME_LOCATION}/stupan/${name}.
 * If a PID file already exists, it's read and it's checked if a process
 * with that PID is currently running. If there is, return that PID, otherwise
 * write our own PID to the file, and return 0.
 *
 * All other possible errors are returned as negative integers, which can
 * be fed to pidfile_errstr. The errors *should not* be negated.
 *
 * @param name Requested PID-file lock. Allows the program to have multiple locks.
 * @return The other PID, a (negative) error code, or 0 on success.
 */
long long int setup_pidfile(QString name);

/**
 * @brief Return a (locale dependent) error message.
 * Should be used together with `setup_pidfile`
 * @param er
 * @return
 */
const QString pidfile_errstr(long long int err);

/**
 * @brief Checks if no-one else holds our lock, and kill ourselves if someone else does.
 *
 * Checks a PID-file. If another process already holds the lock, then display a fatal
 * message box, and then exit the program.
 */
void check_exclusivity();

/**
 * @brief Connects to the database, or die.
 * Prepares Qt for an SQLite database, and attempts to connect to it.
 * Failure results in an error box being shown, and the program terminating.
 *
 * @todo this should also create the database if it doesn't exists.
 *
 * @param db_name Path to the database file.
 */
void setupDatabase(QString db_name);
}

#endif // UTILS


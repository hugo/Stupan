#include "version.h"

#include <QStringList>
#include <QVariant>
#include <QSqlQuery>

const int VERSION_MAJOR = _hVERSION_MAJOR;
const int VERSION_MINOR = _hVERSION_MINOR;
const int VERSION_PATCH = _hVERSION_PATCH;

#define str2(x) #x
#define str(x) str2(x)
#define VERSION str(_hVERSION_MAJOR) "." str(_hVERSION_MINOR) "." str(_hVERSION_PATCH);

const char* version() {
    return VERSION;
}

int32_t version_to_int(QString v) {
    auto lst = v.split(QChar('.'));
    switch (lst.length()) {
    case 1:
        lst.push_back("0");
        [[fallthrough]];
    case 2:
        lst.push_back("0");
        [[fallthrough]];
    case 3:
        break;
    default:
        return -1;
    }

    return lst[0].toUInt() << 20 | lst[1].toUInt() << 10 | lst[2].toUInt();
}

QString int_to_version(int32_t i) {
    uint32_t major, minor, patch;
    major = (i >> 20) & 0x3FF;
    minor = (i >> 10) & 0x3FF;
    patch = i & 0x3FF;
    return QString("%1.%2.%3").arg(major).arg(minor).arg(patch);
}

void set_db_version(QString v) {
    QSqlQuery q;
    q.prepare("PRAGMA user_version = ?");
    int32_t version = version_to_int(v);
    if (version < 0) {
        return;
    }
    q.bindValue(0, QVariant(version));
    q.exec();
}

int get_db_version_raw() {
    QSqlQuery q;
    q.prepare("PRAGMA user_version");
    if (! q.exec()) {
        return -1;
    }
    q.next();
    return q.value(0).toInt();
}

QString get_db_version() {
    return int_to_version(get_db_version_raw());
}

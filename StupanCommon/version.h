#ifndef VERSION_H
#define VERSION_H

#include <QString>
#include <cstdint>

/**
 * @file version.h
 * @brief Program version, and version utilities.
 *
 * The program version is specified in the StupanCommon build file as preprocessor macros.
 * This compilation unit is to "leak" the macros as actual symbols to the rest of the program.
 */

/**
 * @brief Major version of the program.
 */
extern const int VERSION_MAJOR;
/**
 * @brief Minor version of the program.
 */
extern const int VERSION_MINOR;
/**
 * @brief The patch version of the program.
 */
extern const int VERSION_PATCH;

/**
 * @brief A pretty version string.
 * @return on the form major.minor.patch
 */
const char* version();

/**
 * @brief Converts a pretty version string to an integer.
 * The resulting integer has its bottom 10 bits set to the patch,
 * next 10 bits to the minor, and final 10 bits set to the major version.
 * This leaves the topmost 2 bits unset, which we save for future versioning schemes.
 * @param version A pretty version string.
 *   The string can have between 1 and 3 components, and should be period delimited.
 * @return A integer representing the version.
 */
int32_t version_to_int(QString version);

/**
 * @brief Converts a version integer back into a pretty version string.
 * This is the exact inverse of `version_to_int`.
 * @param v Version integer
 * @return A three component pretty version string.
 */
QString int_to_version(int32_t v);

/**
 * @brief Sets the SQLite user version field.
 * @param version Version to set.
 */
void set_db_version(QString version);

/**
 * @brief Fetch the SQLite user version field.
 * @return The raw value stored there.
 */
int get_db_version_raw();

/**
 * @brief Fetch the SQLite user version, and decode it.
 * @return A pretty version string.
 */
QString get_db_version();

#endif // VERSION_H

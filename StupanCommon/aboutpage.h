#ifndef ABOUTPAGE_H
#define ABOUTPAGE_H

#include <QWidget>

namespace Ui {
class AboutPage;
}

/**
 * @brief Controller for the about page widget.
 *
 * This widget should be included as an "About" tab (or similar).
 * It contains the software version, and some other data.
 */
class AboutPage : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief Initialize the page.
     * @param parent Qt parent for cleanup.
     */
    explicit AboutPage(QWidget *parent = nullptr);

    /**
     * @brief Cleans up resoruces.
     */
    ~AboutPage();

private:
    Ui::AboutPage *ui;
};

#endif // ABOUTPAGE_H

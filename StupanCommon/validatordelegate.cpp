#include "validatordelegate.h"

#include <QLineEdit>

ValidatorDelegate::ValidatorDelegate(QValidator* validator, QObject* parent)
    : QStyledItemDelegate(parent)
    , validator(validator)
    { }

QWidget* ValidatorDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem& /*option*/, const QModelIndex& /*index*/) const
{
    QLineEdit* editor = new QLineEdit(parent);
    editor->setValidator(validator);
    return editor;
}


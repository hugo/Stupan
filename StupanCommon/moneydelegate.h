#ifndef MONEYDELEGATE_H
#define MONEYDELEGATE_H

#include <QValidator>
#include "validatordelegate.h"

/**
 * @brief Delegate for money values.
 *
 * Delegate for fields that should be editable as money values
 * Uses the cent equivalent behind the scenes, and edits in the
 * dollar equivalent.
 *
 * Values are internally stored as integers. Limits are only doubles to allow
 * writing it in "dollar" values.
 *
 * Currently assumes that there is always exactly 100 "cents" per "dollar".
 */
class MoneyDelegate
        : public ValidatorDelegate
{
    //Q_OBJECT
public:
    /**
     * @brief Construct the delegate
     * @param parent For Qt cleanup.
     * @param decimals How many decimals should be possible to enter.
     * @param min Minimum allowed value
     * @param max Maximum allowed value
     */
    MoneyDelegate(QObject* parent = nullptr,
                  int decimals = 2,
                  double min = static_cast<double>(-INT_MAX),
                  double max = static_cast<double>(INT_MAX));

    /**
     * @brief Value as a number with decimals
	 *
	 * @todo Actually use locale information
	 *
     * @param value Target value, must be parsable as a double.
     * @param locale Target locale
     * @return The fancy string.
     */
    virtual QString displayText(const QVariant &value, const QLocale &locale) const;

    /**
     * @brief setEditorData
     * @param editor
     * @param index
     */
    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const;

    /**
     * @brief setModelData
     * @param editor
     * @param model
     * @param index
     */
    virtual void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const;

private:
    const int decimals;
};

#endif // MONEYDELEGATE_H

#include "disablingmodel.h"

#include <QtCore>

DisablingModel::DisablingModel(QList<int> disabledColumns,
        QObject* parent,
        QSqlDatabase db)
    : QSqlRelationalTableModel(parent, db)
    , disabledColumns(disabledColumns)
{ }

Qt::ItemFlags DisablingModel::flags(const QModelIndex &index) const
{
    if (disabledColumns.contains(index.column()))
        return this->inactiveFlags;
    else
        return this->activeFlags;
}

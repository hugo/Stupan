<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="aboutpage.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="aboutpage.ui" line="22"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="aboutpage.ui" line="36"/>
        <source>Byggdes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="aboutpage.ui" line="50"/>
        <source>Källkod</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="aboutpage.ui" line="67"/>
        <source>Databasversion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="aboutpage.ui" line="82"/>
        <source>[db dec]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="aboutpage.ui" line="120"/>
        <source>Copyright</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="aboutpage.ui" line="142"/>
        <source># Ättestupans Kassasystem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="aboutpage.cpp" line="18"/>
        <source>yyyy-MM-dd hh:mm</source>
        <comment>När programmet senast byggdes</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="utils.cpp" line="110"/>
        <source>Åtkomst nekad vid försök att kolla om annan process är aktiv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="utils.cpp" line="111"/>
        <source>Låsfil existerar, men kunde inte läsas</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="utils.cpp" line="183"/>
        <source>Misslyckades låsa exklusivitet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="utils.cpp" line="185"/>
        <source>Exklusivitetlåset hålls redan av process %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="utils.cpp" line="202"/>
        <source>Misslyckades ansluta till databasen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="utils.cpp" line="203"/>
        <source>Ingen databasanslutning tillgänglig.
Vänligen kontrollera att den konfigurerade sökvägen är korrekt,
alternativt återskapa databasen</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>

#include "moneydelegate.h"

#include <QLineEdit>

MoneyDelegate::MoneyDelegate(QObject* parent, int decimals, double min, double max)
    : ValidatorDelegate(new QDoubleValidator(min, max, decimals, parent), parent)
    , decimals(decimals)
{ }

QString MoneyDelegate::displayText(const QVariant &value, const QLocale& /*locale*/) const
{
    return QString("%1").arg(value.toDouble() / 100.0, 0, 'f', decimals);
}

void MoneyDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    if (QLineEdit* ed = qobject_cast<QLineEdit*>(editor))
        ed->setText(QString("%1").arg(index.data().toDouble() / 100.0, 0, 'f', decimals));
    else
        ValidatorDelegate::setEditorData(editor, index);
}

void MoneyDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QLocale swedish(QLocale::Swedish);
    if (QLineEdit* ed = qobject_cast<QLineEdit*>(editor)) {
        QVariant data = swedish.toDouble(ed->text()) * 100;
        model->setData(index, data, Qt::EditRole);
    } else
        ValidatorDelegate::setModelData(editor, model, index);
}

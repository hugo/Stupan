#include "qintornullvalidator.h"

QIntOrNullValidator::QIntOrNullValidator(int maximum, int minimum, QObject* parent)
    : QIntValidator(maximum, minimum, parent) {  }

QValidator::State QIntOrNullValidator::validate(QString& str, int& pos) const
{
    if (str == "") return QValidator::Acceptable;
    else return QIntValidator::validate(str, pos);
}

#ifndef QINTORNULLVALIDATOR_H
#define QINTORNULLVALIDATOR_H

#include <QIntValidator>

/**
 * @brief An int validator, which also accepts null values.
 *
 * An extension to QIntValidator, which accepts any string which can
 * be seen as a number, but also accepts the empty string.
 */
class QIntOrNullValidator : public QIntValidator
{
public:
    /**
     * @brief Constructs the validator.
     * @param maximum Minimum allowed value.
     * @param minimum Maximum allowed value.
     * @param parent For Qt Cleanup.
     */
    QIntOrNullValidator(int maximum, int minimum, QObject* parent = nullptr);

    /**
     * @brief Checks if the value is OK.
     * @param str The checked value, should either be empty or an integer.
     * @param pos Passed to the parent integer validator.
     * @return If the value was valid.
     */
    virtual QValidator::State validate(QString& str, int& pos) const;

};

#endif // QINTORNULLVALIDATOR_H

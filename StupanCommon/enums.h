#ifndef ENUMS_H
#define ENUMS_H

namespace SaleStatus {
/**
 * @brief Possible sale statuses for a product.
 *
 * `for_sale` and `not_for_sale` are self explanitory.
 *
 * `hidden` (almost) completely hides the product from all menus,
 * and is used since product's can't be removed (since that would
 * wreck havok on the logs).
 *
 * `system` have previously been used for some internal shenanigans,
 * but isn't any more.
 */
enum SaleStatus : int {
    not_for_sale = 1,
    for_sale = 0,
    hidden = 2,
    system = -1,
};
};

#endif // ENUMS_H

#include "utils.h"

#include <QSqlRecord>
#include <QtCore>
#include <QSqlError>
#include <QStandardPaths>
#include <QMessageBox>
#include <QApplication>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QFileInfo>
#include <QTextStream>
#include <QObject>

#include <cmath>
#include <cstring>
#include <csignal>
#include <cerrno>

int util::round_up_n(int number, int round_value) {
    double n = static_cast<double>(number);
    return ceil(n / round_value) * round_value;
}

void util::requery_sql_view(QTreeView *view, QSqlQueryModel *model)
{
    QModelIndex index = view->currentIndex();
    QString queryStr = model->query().executedQuery();
    model->clear();
    model->setQuery(queryStr);
    view->setCurrentIndex(index);
}

void util::requery_sql_model(QItemSelectionModel *selectionModel, QSqlTableModel* model)
{
    QModelIndex index = selectionModel->currentIndex();
    model->select();
    selectionModel->setCurrentIndex(
                index,
                QItemSelectionModel::Select|QItemSelectionModel::Rows);
}

void util::runSql(const QString& str) {
	QSqlQuery query;
	if (! query.prepare(str)) {
		qDebug() << "Prepare error" << query.lastError();
	}
	if (! query.exec()) {
		qDebug() << "Exec error" << query.lastError();
	}
}

void util::runSqlResource(QFile& file) {
    if (!file.open(QIODevice::ReadOnly|QIODevice::Text)) {
        qDebug() << __FILE__ << __LINE__ << "Failed opening resource file" << file.fileName();
    }

    QTextStream strm(&file);
    QString queries = strm.readAll();

    QSqlQuery query;
    if (!query.exec(queries)) {
        qDebug() << __FILE__
                 << __LINE__
                 << "["
                 << file.fileName()
                 << "]"
                 << query.lastError();
    }
}

void util::runSqlResource(const QString &resourcePath) {
    QFile file(resourcePath);
    runSqlResource(file);
}

QString util::formatMoney(double in) {
    return QString("%1").arg(in, 0, 'f', 2);
}


void util::populateMoneyComboBox(QComboBox* box)
{
    // TODO this combo box isn't updated when new accounts are added.
    box->clear();
    // QSqlQuery accQuery("SELECT id, name FROM money WHERE amount IS NOT NULL");

    QSqlQuery accQuery("SELECT m.id, m.name FROM money m "
                       "LEFT JOIN account_balances b ON m.id = b.account_id "
                       "WHERE m.id != 0");

    while (accQuery.next()) {
        QString name = accQuery.value(1).toString();
        QVariant value = accQuery.value(0);
        box->addItem(name, value);
    }
}

const QString util::pidfile_errstr(long long int err) {
    switch (err) {
    case -EPERM:  return QObject::tr("Åtkomst nekad vid försök att kolla om annan process är aktiv");
    case -EACCES: return QObject::tr("Låsfil existerar, men kunde inte läsas");
    default: return std::strerror(-err);
    }
}

long long int util::setup_pidfile(QString name)
{
    QString path = QStandardPaths::writableLocation(QStandardPaths::RuntimeLocation) + "/stupan/";
    if (! QDir().mkpath(path)) {
        return -errno;
    }

    QFile f(path + "/" + name);

    qDebug() << f;

    if (f.open(QIODevice::ReadOnly|QIODevice::Text)) {
        pid_t old_pid;
        {
            QTextStream in(&f);
            in >> old_pid;
        }
        f.close();

        if (kill(old_pid, 0) == 0) {
            return old_pid;
        } else {
            switch (errno) {
            case ESRCH:
                /* Other process doesn't exist, which is expected (and good) */
                break;
            case EPERM:
            default:
                return -errno;
            }
        }

    } else {
        if (f.exists()) {
            return - EACCES;
        }
    }

    pid_t pid = getpid();
    f.open(QIODevice::WriteOnly|QIODevice::Text);
    {
        QTextStream out(&f);
        out << pid;
    }
    f.close();
    return 0;
}

/*
 * Checks if a pid file exists and points to a running process.
 * If so, displays a message box and kills the program.
 * Otherwise does nothing.
 */
void util::check_exclusivity()
{
    pid_t pid;
    if ((pid = util::setup_pidfile("stupan.pid"))) {
        QMessageBox::critical(nullptr, QObject::tr("Misslyckades låsa exklusivitet"),
                              pid > 0
                                ? QObject::tr("Exklusivitetlåset hålls redan av process %1").arg(pid)
                                : util::pidfile_errstr(pid)
                              );
        qFatal("Couldn't acquire lock");
    }
}

void util::setupDatabase(QString db_name) {

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");

    QFileInfo db_file_info(db_name);
    if (db_file_info.exists() && db_file_info.isFile()) {
        db.setDatabaseName(db_name);
        if(!db.open())
            qDebug() << __FILE__ << __LINE__ << "Database connection failed";
    } else {
        QMessageBox::warning(nullptr, QObject::tr("Misslyckades ansluta till databasen"),
                             QObject::tr("Ingen databasanslutning tillgänglig.\n"
                                         "Vänligen kontrollera att den konfigurerade sökvägen är korrekt,\n"
                                         "alternativt återskapa databasen"));
        QApplication::exit(1);
    }

    util::runSql("PRAGMA FOREIGN_KEYS = ON");
}

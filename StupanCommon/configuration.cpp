#include "configuration.h"

#include <QTextStream>

Configuration::Configuration(QString filename)
{
    QFile file(filename);
    if (file.open(QFile::ReadOnly | QFile::Text)) {
        QTextStream in(&file);
        //qDebug() << f.size() << in.readAll();
        foreach (QString str, in.readAll().split('\n', Qt::SkipEmptyParts)) {
            if (str.size() == 0 || str.at(0) == '#')
                continue;

            QStringList keyvalue = str.split(' ');
            if (keyvalue.at(0) == "port")
                this->port = keyvalue.at(1);
            else if (keyvalue.at(0) == "database")
                this->db_name = keyvalue.at(1);
            else if (keyvalue.at(0) == "double_click")
                this->double_click_enabled = keyvalue.at(1) == "true";
            else if (keyvalue.at(0) == "cash_drawer_count")
                this->cash_drawer_signal_count = keyvalue.at(1).toInt();
        }
    }

}

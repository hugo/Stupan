#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <cstdlib>

#include <QString>
#include <QFile>

/**
 * @brief Static set of configurable variables, and configuration manager.
 *
 * Loads configuration from a file, and populates the struct in question.
 * Anything configurable should be a member here.
 */
class Configuration
{
public:
    /**
     * @brief Loads configuration from the given file.
     * @param filename File to load.
     */
    Configuration(QString filename);

    /**
     * @brief "Path" to the serial port.
     *
     * On *nix systems this will probably be something like `/dev/ttyACM0`,
     * while on Windows it might be COM1
     */
    QString port = "/dev/ttyACM0";

    /**
     * @brief Path to the database.
     *
     * The database is a SQLite file.
     * This usually shouldn't be explicitly set, and defaults to
     * $HOME/.stupan/stupan.db.
     *
     * @todo change to $XDG_DATA_HOME/stupan/stupan.db
     */
    QString db_name = QString::fromLocal8Bit(getenv("HOME")) + "/.stupan/stupan.db";

    /**
     * @brief If double clicking should activate some UI elements.
     *
     * Tree widgets in Qt have the ability to send activation signals on double click.
     * This toggles if that is turned on or off. The reason for turning it off is to
     * reduce the chance of an accidental click, especially in the case of lag spikes.
     */
    bool double_click_enabled = false;

    /**
     * @brief How many pulses should be sent to the cash drawer.
     *
     * Ideally this should only ever be 1, but some serial interfaces don't
     * seem to behave. Sending multiple pulses sometimes "fixes" this.
     */
    int cash_drawer_signal_count = 1;
};

#endif // CONFIGURATION_H

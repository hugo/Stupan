#include "maybevalidatordelegate.h"

#include <QLineEdit>

MaybeValidatorDelegate::MaybeValidatorDelegate(QValidator* validator, QObject* parent)
    : ValidatorDelegate (validator, parent) { }

void MaybeValidatorDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QLineEdit* ed = qobject_cast<QLineEdit*>(editor);
    QString str = ed->text();
    // empty string is null, fuck you
    model->setData(index, str == "" ? QVariant() // QVariant(QMetaType::QString)
                                    : str);
}

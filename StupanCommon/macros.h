#ifndef MACROS_H
#define MACROS_H

#include <vector>
#include <utility>
#include <QAbstractItemModel>
#include <QAbstractItemView>
#include <QAbstractItemDelegate>
#include <QString>

#include <initializer_list>

/* Takes a QSqlQueryModel, and any amount of integer, string pairs
 * Calls the models setHeaderData method for each pair.
 */
#define SET_HEADER_DATA(MODEL, ...) set_header_data((MODEL), { __VA_ARGS__ })
[[maybe_unused]]
static inline void set_header_data(QAbstractItemModel *model, std::vector<std::pair<int, QString>> args) {
    for (auto &v : args) {
        model->setHeaderData(v.first, Qt::Horizontal, v.second);
    }
}

/* Takes a QTableView, and any omount of integer, QAbstractItemDelegate pointer pairs
 * Calls the views setItemDelegateForColumn for each pair.
 */
#define SET_COL_DELEGATORS(VIEW, ...) set_col_delegators((VIEW), { __VA_ARGS__ })
[[maybe_unused]]
static inline void set_col_delegators(QAbstractItemView *view, std::vector<std::pair<int, QAbstractItemDelegate*>> args) {
    for (auto v : args) {
        view->setItemDelegateForColumn(v.first, v.second);
    }
}

/* Takes a QTableView, and any amount of integers.
 * Each integer is passed to the views hideColumn method
 */
#define HIDE_COLUMNS(VIEW, ...) hide_columns((VIEW), { __VA_ARGS__ })
template<typename T>
[[maybe_unused]]
static inline void hide_columns(T *view, std::vector<int> args) {
    for (auto n : args) {
        /* This is independently implemented by QTreeView and QTableView */
        view->hideColumn(n);
    }
}

#endif // MACROS_H

#ifndef COMBOBOXITEMDELEGATE_H
#define COMBOBOXITEMDELEGATE_H

#include <QStyledItemDelegate>
#include <QMap>

/**
 * @brief A item delegate displaying combo boxes.
 *
 * Allows for combo boxes (dropdown menus) in tables.
 *
 * While the keys _are_ int's, it's recommended to use an enum
 * which can decay into integers.
 * \include comboboxitemdelegate.cpp
 */
class ComboBoxItemDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    /**
     * @brief Constructor for ComboBoxItemDelegate
     * @param options Values for the combo box.
     *     The map's keys will be the underlying values of the field,
     *     while the strings will be what's displayed.
     * @param parent Qt parent for memory cleanup.
     */
    ComboBoxItemDelegate(QMap<int, QString> options, QObject* parent = 0);
    // ~ComboBoxItemDelegate();

    /**
     * @brief Allocates all needed resources.
     * See QStyledItemDelegate for more information.
     * @param parent Qt parent for memory cleanup.
     * @param option
     * @param index
     * @return The editor widget.
     */
    virtual QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const;
    /**
     * @brief gets the value from the combo box, and updates ourselves.
     * @param editor
     * @param index
     */
    virtual void setEditorData(QWidget* editor, const QModelIndex& index) const;
    /**
     * @brief setModelData
     * @param editor
     * @param model
     * @param index
     */
    virtual void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const;
    /**
     * @brief displayText
     * @param value
     * @param locale
     * @return
     */
    virtual QString displayText(const QVariant &value, const QLocale &locale) const;

private:
    QMap<int, QString> options;
    QString get_index_string(int) const;
};

#endif // COMBOBOXITEMDELEGATE_H

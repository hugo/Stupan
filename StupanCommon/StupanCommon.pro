QT += core gui sql serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

DEFINES += _hVERSION_MAJOR=1
DEFINES += _hVERSION_MINOR=17
DEFINES += _hVERSION_PATCH=2

TEMPLATE = lib
CONFIG += staticlib
DEFINES += COMMON_LIBRARY

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    aboutpage.cpp \
    comboboxitemdelegate.cpp \
    validatordelegate.cpp \
    moneydelegate.cpp \
    disablingmodel.cpp \
    qintornullvalidator.cpp \
    maybevalidatordelegate.cpp \
    cashdrawer.cpp \
    configuration.cpp \
    utils.cpp \
    version.cpp

HEADERS += \
    aboutpage.h \
    enums.h \
    macros.h \
    comboboxitemdelegate.h \
    validatordelegate.h \
    moneydelegate.h \
    disablingmodel.h \
    qintornullvalidator.h \
    maybevalidatordelegate.h \
    cashdrawer.h \
    configuration.h \
    utils.h \
    version.h

# Default rules for deployment.
unix {
    target.path = /usr/lib
}
!isEmpty(target.path): INSTALLS += target

FORMS += \
    aboutpage.ui

TRANSLATIONS += stupan_dk.ts

#ifndef VALIDATORDELEGATE_H
#define VALIDATORDELEGATE_H

#include <QItemDelegate>
#include <QStyledItemDelegate>
#include <QValidator>

/**
 * @brief A item delegate which also validates the input
 *
 * When editing, the edit field will be given a line edit with the given validator
 * attached. The field can't be submitted unless the validator accepts the currently
 * typed string.
 */
class ValidatorDelegate : public QStyledItemDelegate
{
    //Q_OBJECT
public:
    /**
     * @brief Constructs the delegate.
     * @param validator The validator which will be used
     * @param parent Qt Parent for cleanup.
     */
    ValidatorDelegate(QValidator* validator, QObject* parent = nullptr);
    /**
     * @brief Allocates the actuall resources.
     * @param parent
     * @param option
     * @param index
     * @return A QLineEditor with the given validator attached.
     */
    virtual QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;

private:
    const QValidator* validator;
};

#endif // VALIDATORDELEGATE_H

#include "cashdrawer.h"

#include <stdlib.h>

CashDrawer::CashDrawer(QString portname, int times)
    : times(times)
{
    this->serialPort.setPortName(portname);
    this->serialPort.setBaudRate(QSerialPort::Baud9600);

    QObject::connect(&this->serialPort, &QIODevice::bytesWritten, this, &CashDrawer::afterSerialWrite);
}

void CashDrawer::open_till() {
    if (!this->serialPort.open(QIODevice::WriteOnly)) {
        qDebug() << "Can't open serial port"
                 << this->serialPort.portName()
                 << this->serialPort.error();
        return;
    }

    for (int i = 0; i < std::min(1000, std::max(this->times, 1)); i++) {
        this->serialPort.write("1");
    }
}

void CashDrawer::afterSerialWrite(qint64 /*bytes*/) {
    this->serialPort.close();
}

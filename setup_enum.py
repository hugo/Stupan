#!/usr/bin/env python3

"""
Build C++ headers from Qt Resource files.

Each command line argument should be a path to a Qt Resource file.
Each file will be scanned for names ending in `.sql`, which each will
be sourced. The result will be output to `db_enum.h` in the current
working directory.

TODO Currently it also sources `../../create-db.sql` (hard-coded).
"""

import xml.etree.ElementTree as ET
import sys
import os
import os.path

from sqlite_to_cpp import sqlite_to_cpp


def main():
    """Entry point of program."""
    here = os.getcwd()

    print(sys.argv, file=sys.stderr)

    query = ''

    with open('../../create-db.sql') as f:
        query += f.read()

    for file in sys.argv[1:]:
        print('Handling', file)
        tree = ET.parse(file)
        root = tree.getroot()

        here = os.getcwd()
        try:
            os.chdir(os.path.dirname(file))

            for qresource in root.findall('qresource'):
                # print(qresource.attrib['prefix'])
                for file in qresource.findall('file'):
                    if not file.text.endswith('.sql'):
                        continue
                    with open(file.text) as f:
                        query += f.read() + ';\n'

        finally:
            os.chdir(here)

    with open(os.path.join(here, 'db_enum.h'), 'w') as f:
        print(sqlite_to_cpp(schema=query,
                            warning_directives=True,
                            # TODO shouldn't be needed. Check for bugs upstream
                            namespace="DB"),
              file=f)


if __name__ == '__main__':
    main()
